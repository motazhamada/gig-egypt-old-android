package com.sts.gig.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;

import com.sts.gig.activities.SettingsActivity;

import java.util.Locale;

public class LanguageManager {

    private Context context;
    private Locale locale;

    public LanguageManager(Context context) {
        this.context = context;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public void setLanguage()
    {
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(locale);
        res.updateConfiguration(conf, dm);

        SharedPreferences.Editor sharedPreferencesEditor = context.getSharedPreferences("language_settings", Context.MODE_PRIVATE).edit();
        sharedPreferencesEditor.putString("lang", locale.getLanguage());
        sharedPreferencesEditor.putString("country", locale.getCountry());
        sharedPreferencesEditor.apply();

    }

    public void getLanguage()
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("language_settings", Context.MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "en");
        String country = sharedPreferences.getString("country", "US");

        locale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(locale);
        res.updateConfiguration(conf, dm);
    }
}
