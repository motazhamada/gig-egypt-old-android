package com.sts.gig.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.sts.gig.activities.LoginActivity;

import static android.content.Context.MODE_PRIVATE;

public class UserData {

    public void Set_UserToken(Context activity, String UserToken,boolean ActivityLogin) {
        SharedPreferences.Editor sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE).edit();
        sharedPref.putString("UserToken", UserToken);
        sharedPref.putBoolean("ActivityLogin", ActivityLogin);
        sharedPref.apply();
        sharedPref.commit();
    }

    public boolean Check_UserToken(Context activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("UserToken", null);
        return restoredText != null;
    }


    public String Get_UserToken(Context activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("UserToken", null);
        if (restoredText != null) {
            return restoredText;
        }
        return "No User Token";
    }

    public boolean isLogin(Context activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE);
        return sharedPref.getBoolean("ActivityLogin", false);
    }

    public void Clear_UserData(Context activity) {
            SharedPreferences.Editor sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE).edit();
            sharedPref.clear();
            sharedPref.apply();
    }

    public void isLogout(Activity activity) {

        if (!isLogin(activity)) {
            Clear_UserData(activity);
            Intent intent = new Intent(activity, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            activity.finish();
        }
    }
}
