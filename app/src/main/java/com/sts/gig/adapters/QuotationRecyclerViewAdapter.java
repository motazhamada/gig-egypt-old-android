package com.sts.gig.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.activities.HomeActivity;
import com.sts.gig.activities.LoginActivity;
import com.sts.gig.activities.QuotationActivity;
import com.sts.gig.activities.QuotationDetailsActivity;
import com.sts.gig.activities.SignUpActivity;
import com.sts.gig.classes.Quotation;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.UserData;

import java.util.List;
import java.util.Objects;

public class QuotationRecyclerViewAdapter extends RecyclerView.Adapter<QuotationRecyclerViewAdapter.ViewHolder> {

    private List<Quotation> mData;
    private LayoutInflater mInflater;
    private Context mContext;
    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private UserData userData;
    private View mView;

    public static int selectedQuot;
    public static Context mContext_2;
    public static View mView_2;

    // data is passed into the constructor
    public QuotationRecyclerViewAdapter(Context context, List<Quotation> data, View view) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mView = view;
        mContext_2=context;
        mView_2=view;
    }


    // inflates the row layout from xml when needed
    @Override
    public QuotationRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_row_quotation, parent, false);
        return new QuotationRecyclerViewAdapter.ViewHolder(view);
    }

    public void add(int position, Quotation item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Quotation item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final QuotationRecyclerViewAdapter.ViewHolder holder, int position) {
        final Quotation Quotation = mData.get(position);
        Ion.with(mContext)
                .load(Quotation.getImageUrl())
                .withBitmap()
                .placeholder(R.drawable.ic_image_black_24dp)
                .error(R.drawable.ic_broken_image_black_24dp)
                .intoImageView(holder.mQuotationImageView);

        holder.mQuotationTextView.setText(Quotation.getName());

        holder.mQuotationCardViewRipple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, QuotationDetailsActivity.class);
                intent.putExtra("name", Quotation.getName());
                intent.putExtra("text", Quotation.getText());
                intent.putExtra("imageUrl", Quotation.getImageUrl());
                selectedQuot=Quotation.getId();
                mContext.startActivity(intent);

            }
        });
       /* holder.mQuotationRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(LoginActivity.testActivation==-1)
                {
                    Intent intent = new Intent(v.getContext(), SignUpActivity.class);
                    mContext.startActivity(intent);
                }


               else {
                    MakeRequest(Quotation.getId());
                }
            }
        }); */
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    public Quotation getItem(int id) {
        return mData.get(id);
    }

    private void MakeRequest(final int quotation_id) {
        try {
            if (internetConnection.isNetworkAvailable(mContext)) {
                // Instantiate the RequestQueue.
                Ion.with(mContext)
                        .load(mContext.getString(R.string.Resource_Address) + mContext.getString(R.string.Make_Quotations_Request_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(mContext))
                        .setBodyParameter("quotation_id", String.valueOf(quotation_id))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);

                                        if (Objects.equals(tokenRequest.getSuccess(), "true")) {
                                            Snackbar snackbar = Snackbar
                                                    .make(mView, "Request Sent", Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        } else {
                                            Snackbar snackbar = Snackbar
                                                    .make(mView, "Request Failed", Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(mContext.getString(R.string.TAG), "Other Services Adapter 1 : " + error.getMessage());
                                    }
                                }
                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(mView, "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Try Again", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                MakeRequest(quotation_id);
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            Snackbar snackbar = Snackbar
                    .make(mView, "there is an Error try again later.", Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(mContext.getString(R.string.TAG), "Other Services Adapter 2 Exception = " + String.valueOf(e));
        }

    }

    public  void MakeRequest_2(final int quotation_id)
    {
        try {
            if (internetConnection.isNetworkAvailable(mContext)) {
                // Instantiate the RequestQueue.
                Ion.with(mContext)
                        .load(mContext.getString(R.string.Resource_Address) + mContext.getString(R.string.Make_Quotations_Request_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(mContext))
                        .setBodyParameter("quotation_id", String.valueOf(quotation_id))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);

                                        if (Objects.equals(tokenRequest.getSuccess(), "true")) {
                                            Snackbar snackbar = Snackbar
                                                    .make(mView, "Request Sent", Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        } else {
                                            Snackbar snackbar = Snackbar
                                                    .make(mView, "Request Failed", Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(mContext.getString(R.string.TAG), "Other Services Adapter 1 : " + error.getMessage());
                                    }
                                }
                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(mView, "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Try Again", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                MakeRequest(quotation_id);
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            Snackbar snackbar = Snackbar
                    .make(mView, "there is an Error try again later.", Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(mContext.getString(R.string.TAG), "Other Services Adapter 2 Exception = " + String.valueOf(e));
        }

    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        MaterialRippleLayout mQuotationCardViewRipple;
        ImageView mQuotationImageView;
        TextView mQuotationTextView;
       // Button mQuotationRequestButton;

        ViewHolder(View itemView) {
            super(itemView);
            mQuotationCardViewRipple = itemView.findViewById(R.id.quotation_cardViewRipple);
            mQuotationImageView = itemView.findViewById(R.id.iv_quotation_image);
            mQuotationTextView = itemView.findViewById(R.id.tv_quotation_text);
            userData = new UserData();
            internetConnection = new InternetConnection();
        }

    }

}
