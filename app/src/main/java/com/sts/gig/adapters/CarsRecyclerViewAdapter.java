package com.sts.gig.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.sts.gig.classes.Car;
import com.sts.gig.R;

import java.util.List;

public class CarsRecyclerViewAdapter extends RecyclerView.Adapter<CarsRecyclerViewAdapter.ViewHolder> {

    private List<Car> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public CarsRecyclerViewAdapter(Context context, List<Car> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_row_car, parent, false);
        return new ViewHolder(view);
    }
    public void add(int position, Car item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }
    public void remove(Car item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Car car = mData.get(position);
        holder.brandTextView.setText(car.getBrand());
        holder.modelTextView.setText(car.getModel());
        holder.plateNumberTextView.setText(car.getPlate_Number());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        MaterialRippleLayout car_cardViewRipple;
        TextView brandTextView,modelTextView,plateNumberTextView;

        ViewHolder(View itemView) {
            super(itemView);
            car_cardViewRipple = itemView.findViewById(R.id.car_cardViewRipple_cars_row);
            brandTextView = itemView.findViewById(R.id.brand_textView_cars_row);
            modelTextView = itemView.findViewById(R.id.model_textView_cars_row);
            plateNumberTextView = itemView.findViewById(R.id.plateNumber_textView_cars_row);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Car getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}