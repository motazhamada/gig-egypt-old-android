package com.sts.gig.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.News;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.UserData;

import java.util.List;

public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder> {

    private List<News> mData;
    private LayoutInflater mInflater;
    private Context mContext;
    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private UserData userData;
    private View mView;
    // data is passed into the constructor
    public NewsRecyclerViewAdapter(Context context, List<News> data, View view) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mView = view;
    }


    // inflates the row layout from xml when needed
    @Override
    public NewsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_row_news, parent, false);
        return new NewsRecyclerViewAdapter.ViewHolder(view);
    }

    public void add(int position, News item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(News item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(NewsRecyclerViewAdapter.ViewHolder holder, int position) {
        final News news = mData.get(position);
        Ion.with(mContext)
                .load(news.getImageUrl())
                .withBitmap()
                .placeholder(R.drawable.ic_image_black_24dp)
                .error(R.drawable.ic_broken_image_black_24dp)
                .intoImageView(holder.mNewsImageView);

        holder.mNewsTitleTextView.setText(news.getTitle());
        holder.mNewsTextTextView.setText(news.getText());

//                holder.mNewsCardViewRipple.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        MakeRequest(news.getId());
//                    }
//                });

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    public News getItem(int id) {
        return mData.get(id);
    }

//    private void MakeRequest(final int other_service_id) {
//        try {
//            if (internetConnection.isNetworkAvailable(mContext)) {
//                // Instantiate the RequestQueue.
//                Ion.with(mContext)
//                        .load(mContext.getString(R.string.Resource_Address) + mContext.getString(R.string.Make_Quotations_Request_Address))
//                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(mContext))
//                        .setBodyParameter("other_service_id", String.valueOf(other_service_id))
//                        .asJsonObject()
//                        .setCallback(new FutureCallback<JsonObject>() {
//                            @Override
//                            public void onCompleted(Exception e, JsonObject result) {
//
//                                if (result != null && !Objects.equals(result, "[]")) {
//                                    try {
//                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
//                                        tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
//
//                                        if (Objects.equals(tokenRequest.getSuccess(), "true")) {
//                                            Snackbar snackbar = Snackbar
//                                                    .make(mView, "Request Sent", Snackbar.LENGTH_LONG);
//                                            snackbar.show();
//                                        } else {
//                                            Snackbar snackbar = Snackbar
//                                                    .make(mView, "Request Failed", Snackbar.LENGTH_LONG);
//                                            snackbar.show();
//                                        }
//                                    } catch (Exception error) {
//                                        Log.e(mContext.getString(R.string.TAG), "Other Services Adapter 1 : " + error.getMessage());
//                                    }
//                                }
//                            }
//                        });
//            } else {
//                Snackbar snackbar = Snackbar
//                        .make(mView, "No Internet Connection", Snackbar.LENGTH_LONG)
//                        .setAction("Try Again", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                MakeRequest(other_service_id);
//                            }
//                        });
//                snackbar.show();
//            }
//        } catch (Exception e) {
//            Snackbar snackbar = Snackbar
//                    .make(mView, "there is an Error try again later.", Snackbar.LENGTH_LONG);
//            snackbar.show();
//            Log.e(mContext.getString(R.string.TAG), "Other Services Adapter 2 Exception = " + String.valueOf(e));
//        }
//
//    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        MaterialRippleLayout mNewsCardViewRipple;
        ImageView mNewsImageView;
        TextView mNewsTitleTextView;
        TextView mNewsTextTextView;

        ViewHolder(View itemView) {
            super(itemView);
            mNewsCardViewRipple = itemView.findViewById(R.id.news_cardViewRipple);
            mNewsImageView = itemView.findViewById(R.id.iv_news_image);
            mNewsTitleTextView = itemView.findViewById(R.id.tv_news_title);
            mNewsTextTextView= itemView.findViewById(R.id.tv_news_text);
            userData = new UserData();
            internetConnection = new InternetConnection();
        }

    }

}
