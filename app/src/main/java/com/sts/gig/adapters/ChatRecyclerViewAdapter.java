package com.sts.gig.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sts.gig.R;
import com.sts.gig.classes.Chat;
import com.sts.gig.classes.Message;

import java.util.List;
import java.util.Objects;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int USER_TYPE = 0;
    public static final int ADMIN_TYPE = 1;
    private Chat mChat;
    private Context mContext;
    private List<Message> mData;
    private LayoutInflater mInflater;

    // data is passed into the constructor
    public ChatRecyclerViewAdapter(Context context, Chat data) {
        this.mInflater = LayoutInflater.from(context);
        this.mChat = data;
        this.mData = data.getMessages();
        this.mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (Objects.equals(mData.get(position).getSenderType(), "user")) {
            return USER_TYPE;
        } else {
            return ADMIN_TYPE;
        }
    }


    // inflates the row layout from xml when needed
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == USER_TYPE) {
            view = mInflater.inflate(R.layout.my_message, parent, false);
            return new MyMessagesViewHolder(view);
        } else {
            view = mInflater.inflate(R.layout.other_message, parent, false);
            return new OtherMessagesViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Message message = mData.get(position);
        try {
            switch (holder.getItemViewType()) {
                case USER_TYPE:
                    final MyMessagesViewHolder myMessagesViewHolder = (MyMessagesViewHolder) holder;
                    myMessagesViewHolder.mMyMessageTextView.setText(message.getContent());
                    myMessagesViewHolder.mMyMessageTimeTextView.setText(message.getUpdated_at());
                    break;

                default:
                    final OtherMessagesViewHolder otherMessagesViewHolder = (OtherMessagesViewHolder) holder;
                    otherMessagesViewHolder.mOtherMessageSenderTextView.setText(message.getSenderType());
                    otherMessagesViewHolder.mOtherMessageTextView.setText(message.getContent());
                    otherMessagesViewHolder.mOtherMessageTimeTextView.setText(message.getUpdated_at());
                    break;
            }
        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), "FixtureRecyclerViewAdapter 1 = " + String.valueOf(e));
        }

    }

    public void add(int position, Message item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Message item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }


    // total num1ber of rows
    @Override
    public int getItemCount() {
        if(mData!=null)
        return mData.size();
        else
            return 0;
    }

    // convenience method for getting data at click position
    public Message getItem(int id) {
        return mData.get(id);
    }


    private class MyMessagesViewHolder extends RecyclerView.ViewHolder {
        TextView mMyMessageTextView,mMyMessageTimeTextView;
        public MyMessagesViewHolder(View view) {
            super(view);
            mMyMessageTextView = view.findViewById(R.id.tv_my_message);
            mMyMessageTimeTextView = view.findViewById(R.id.tv_my_message_time);
        }
    }

    private class OtherMessagesViewHolder extends RecyclerView.ViewHolder {
        TextView mOtherMessageSenderTextView,mOtherMessageTextView,mOtherMessageTimeTextView;
        public OtherMessagesViewHolder(View view) {
            super(view);
            mOtherMessageSenderTextView= view.findViewById(R.id.tv_other_message_sender);
            mOtherMessageTextView = view.findViewById(R.id.tv_other_message);
            mOtherMessageTimeTextView = view.findViewById(R.id.tv_other_message_time);
        }
    }
}
