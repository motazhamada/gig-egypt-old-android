package com.sts.gig.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.sts.gig.R;
import com.sts.gig.classes.Accident;

import java.util.List;

public class AccidentsRecyclerViewAdapter extends RecyclerView.Adapter<AccidentsRecyclerViewAdapter.ViewHolder> {

    private List<Accident> mData;
    private LayoutInflater mInflater;
    private AccidentsRecyclerViewAdapter.ItemClickListener mClickListener;

    // data is passed into the constructor
    public AccidentsRecyclerViewAdapter(Context context, List<Accident> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }



    // inflates the row layout from xml when needed
    @Override
    public AccidentsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_row_accident, parent, false);
        return new AccidentsRecyclerViewAdapter.ViewHolder(view);
    }
    public void add(int position, Accident item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }
    public void remove(Accident item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(AccidentsRecyclerViewAdapter.ViewHolder holder, int position) {
        Accident Accident = mData.get(position);
        holder.mAccidentIDTextView.setText(String.format("Accident ID: %1$s", Accident.getId()));
        holder.mPlateNumberTextView.setText(String.format("Plate Number: %1$s", Accident.getPlateNumber()));
        holder.mDateTextView.setText(String.format("Date: %1$s", Accident.getAccidentDate()));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        MaterialRippleLayout Accident_ViewRipple;
        TextView mAccidentIDTextView, mDateTextView, mPlateNumberTextView;

        ViewHolder(View itemView) {
            super(itemView);
            Accident_ViewRipple = itemView.findViewById(R.id.accident_cardViewRipple_accidents_row);
            mAccidentIDTextView = itemView.findViewById(R.id.carName_textView_accidents_row);
            mPlateNumberTextView = itemView.findViewById(R.id.tv_plateNumber_accidents_row);
            mDateTextView = itemView.findViewById(R.id.date_textView_accidents_row);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Accident getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(AccidentsRecyclerViewAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}