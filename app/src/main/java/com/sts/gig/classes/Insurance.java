package com.sts.gig.classes;

public class Insurance {

    private String id;
    private String accident_id;
    private String insuranceDate;
    private String type;
    private String previewBeforeReform;
    private String previewBeforeDate;
    private String previewAfterReform;
    private String previewAfterDate;
    private String reportAccident;
    private String reportDate;
    private String checkDelivery;
    private String checkDate;
    private String created_at;
    private String updated_at;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccident_id() {
        return accident_id;
    }

    public void setAccident_id(String accident_id) {
        this.accident_id = accident_id;
    }

    public String getInsuranceDate() {
        return insuranceDate;
    }

    public void setInsuranceDate(String insuranceDate) {
        this.insuranceDate = insuranceDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPreviewBeforeReform() {
        return previewBeforeReform;
    }

    public void setPreviewBeforeReform(String previewBeforeReform) {
        this.previewBeforeReform = previewBeforeReform;
    }

    public String getPreviewBeforeDate() {
        return previewBeforeDate;
    }

    public void setPreviewBeforeDate(String previewBeforeDate) {
        this.previewBeforeDate = previewBeforeDate;
    }

    public String getPreviewAfterReform() {
        return previewAfterReform;
    }

    public void setPreviewAfterReform(String previewAfterReform) {
        this.previewAfterReform = previewAfterReform;
    }

    public String getPreviewAfterDate() {
        return previewAfterDate;
    }

    public void setPreviewAfterDate(String previewAfterDate) {
        this.previewAfterDate = previewAfterDate;
    }

    public String getReportAccident() {
        return reportAccident;
    }

    public void setReportAccident(String reportAccident) {
        this.reportAccident = reportAccident;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getCheckDelivery() {
        return checkDelivery;
    }

    public void setCheckDelivery(String checkDelivery) {
        this.checkDelivery = checkDelivery;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
