package com.sts.gig.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TokenRequestMakeYear extends TokenRequest {
    @SerializedName("data")
    private List<MakeYear> makeYears;


    public List<MakeYear> getMakeYears() {
        return makeYears;
    }

    public void setMakeYears(List<MakeYear> makeYears) {
        this.makeYears = makeYears;
    }
}
