package com.sts.gig.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TokenRequestQuotation extends TokenRequest {
    @SerializedName("data")
    private List<Quotation> quotations;

    public List<Quotation> getQuotations() {
        return quotations;
    }

    public void setQuotations(List<Quotation> quotations) {
        this.quotations = quotations;
    }
}
