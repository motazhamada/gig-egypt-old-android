package com.sts.gig.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TokenRequestCars extends TokenRequest {

    @SerializedName("data")
    private List<Car> Cars;

    public List<Car> getCars() {
        return Cars;
    }

    public void setCars(List<Car> cars) {
        Cars = cars;
    }
}


