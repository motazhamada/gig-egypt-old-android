package com.sts.gig.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TokenRequestNews extends TokenRequest {

    @SerializedName("data")
    private List<News> news;

    public List<News> getNews() {
        return news;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }
}