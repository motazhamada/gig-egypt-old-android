package com.sts.gig.classes;

import com.google.gson.annotations.SerializedName;

public class Car {
    private String active;
    private String created_at;
    private String updated_at;
    @SerializedName("id")
    private String Car_ID;
    @SerializedName("chassisNumber")
    private String Chassis;
    @SerializedName("brand")
    private String Brand;
    @SerializedName("model")
    private String Model;
    @SerializedName("makeYear")
    private String Make_Year;
    @SerializedName("plateNumber")
    private String Plate_Number;
    @SerializedName("user_id")
    private String User_ID;

    public String getCar_ID() {
        return Car_ID;
    }

    public void setCar_ID(String car_ID) {
        Car_ID = car_ID;
    }

    public String getChassis() {
        return Chassis;
    }

    public void setChassis(String chassis) {
        Chassis = chassis;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getMake_Year() {
        return Make_Year;
    }

    public void setMake_Year(String make_Year) {
        Make_Year = make_Year;
    }

    public String getPlate_Number() {
        return Plate_Number;
    }

    public void setPlate_Number(String plate_Number) {
        Plate_Number = plate_Number;
    }

    public String getUser_ID() {
        return User_ID;
    }

    public void setUser_ID(String user_ID) {
        User_ID = user_ID;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
