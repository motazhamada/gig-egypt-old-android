package com.sts.gig.classes;

import com.google.gson.annotations.SerializedName;

public class TokenRequestCarCount extends TokenRequest {
    @SerializedName("data")
    private String CarCount;

    public String getCarCount() {
        return CarCount;
    }

    public void setCarCount(String carCount) {
        CarCount = carCount;
    }
}
