package com.sts.gig.classes;

import com.google.gson.annotations.SerializedName;

public class TokenRequestChat extends TokenRequest {
    @SerializedName("data")
    private
    Chat chats;


    public Chat getChats() {
        return chats;
    }

    public void setChats(Chat chats) {
        this.chats = chats;
    }
}
