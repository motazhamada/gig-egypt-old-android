package com.sts.gig.classes;

import java.util.List;

public class Error {
    private List<String> phone;
    private List<String> email;
    private List<String> nationalId;
    private List<String> username;
    private List<String> password;
    private List<String> newPassword;
    private List<String> c_password;

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<String> getUsername() {
        return username;
    }

    public void setUsername(List<String> username) {
        this.username = username;
    }

    public List<String> getPassword() {
        return password;
    }

    public void setPassword(List<String> password) {
        this.password = password;
    }

    public List<String> getC_password() {
        return c_password;
    }

    public void setC_password(List<String> c_password) {
        this.c_password = c_password;
    }

    public List<String> getNationalId() {
        return nationalId;
    }

    public void setNationalId(List<String> nationalId) {
        this.nationalId = nationalId;
    }

    public List<String> getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(List<String> newPassword) {
        this.newPassword = newPassword;
    }
}
