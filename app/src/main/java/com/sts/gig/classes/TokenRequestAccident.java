package com.sts.gig.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TokenRequestAccident extends TokenRequest {
    @SerializedName("data")
    private List<Accident> accidents;

    public List<Accident> getAccidents() {
        return accidents;
    }

    public void setAccidents(List<Accident> accidents) {
        this.accidents = accidents;
    }
}
