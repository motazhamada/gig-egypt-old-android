package com.sts.gig.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;

import com.dd.processbutton.iml.ActionProcessButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.Error;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.sv_sign_up)
    ScrollView mScrollViewContainer;
    private ActionProcessButton signUp_button;
    private TextInputLayout usernameLayout_signup, phoneLayout_signup, emailLayout_signup, idNumberLayout_signup, passwordLayout_signup, password2Layout_signup;
    private EditText username_signup, phone_signup, email_signup, idNumber_signup, password_signup, password2_signup;

    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private UserData userData;

    @Override
    protected void onStart() {
        super.onStart();
        signUp_button.setMode(ActionProcessButton.Mode.PROGRESS);
        signUp_button.setProgress(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(SignUpActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        userData = new UserData();
        internetConnection = new InternetConnection();

        usernameLayout_signup = findViewById(R.id.usernameLayout_signup);
        username_signup = findViewById(R.id.username_signup);

        phoneLayout_signup = findViewById(R.id.phoneLayout_signup);
        phone_signup = findViewById(R.id.phone_signup);

        emailLayout_signup = findViewById(R.id.emailLayout_signup);
        email_signup = findViewById(R.id.email_signup);

        idNumberLayout_signup = findViewById(R.id.idNumberLayout_signup);
        idNumber_signup = findViewById(R.id.idNumber_signup);

        passwordLayout_signup = findViewById(R.id.passwordLayout_signup);
        password_signup = findViewById(R.id.password_signup);

        password2_signup = findViewById(R.id.password2_signup);
        password2Layout_signup = findViewById(R.id.password2Layout_signup);


        signUp_button = findViewById(R.id.signUp_button_signup);
        signUp_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSignUp();
            }
        });
    }


    private void attemptSignUp() {
        // Reset errors.
        usernameLayout_signup.setError(null);
        emailLayout_signup.setError(null);
        idNumberLayout_signup.setError(null);
        phoneLayout_signup.setError(null);
        passwordLayout_signup.setError(null);
        password2Layout_signup.setError(null);

        // Store values at the time of the signup attempt.
        String username = username_signup.getText().toString();
        String email = email_signup.getText().toString();
        String idNumber = idNumber_signup.getText().toString();
        String phone = phone_signup.getText().toString();
        String password = password_signup.getText().toString();
        String password2 = password2_signup.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password2, if the user entered one.
        if (!Objects.equals(password, password2)) {
            password2Layout_signup.setError(getString(R.string.error_mismatch_passwords));
            focusView = password2_signup;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            password2Layout_signup.setError(getString(R.string.error_invalid_password));
            focusView = password_signup;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(phone)) {
            phoneLayout_signup.setError(getString(R.string.error_field_required));
            focusView = phone_signup;
            cancel = true;
        } else if (!isPhoneNumberValid(phone)) {
            phoneLayout_signup.setError(getString(R.string.error_invalid_phone_number));
            focusView = phone_signup;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailLayout_signup.setError(getString(R.string.error_field_required));
            focusView = email_signup;
            cancel = true;
        } else if (!isEmailValid(email)) {
            emailLayout_signup.setError(getString(R.string.error_invalid_email));
            focusView = email_signup;
            cancel = true;
        }
        if (TextUtils.isEmpty(idNumber)) {
            idNumberLayout_signup.setError(getString(R.string.error_field_required));
            focusView = idNumber_signup;
            cancel = true;
        } else if (!isIdNumberValid(idNumber)) {
            idNumberLayout_signup.setError(getString(R.string.error_invalid_idNumber));
            focusView = idNumber_signup;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            usernameLayout_signup.setError(getString(R.string.error_field_required));
            focusView = username_signup;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt signup and focus the first
            // form field with an error.
            focusView.requestFocus();
            signUp_button.setProgress(-1);
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user signup attempt.
//            showProgress(true);
            signUp_button.setProgress(0);
            SignUp(username, email, idNumber, phone, password);
        }
    }

    private boolean isIdNumberValid(String idNumber) {
        return idNumber.length() == 14;
    }


    private boolean isEmailValid(String email) {

        return email.contains("@");
    }


    private boolean isPhoneNumberValid(String phoneNumber) {

        return phoneNumber.length() > 10;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 2;
    }

    protected void SignUp(final String username, final String email, final String idNumber, final String phone, final String password) {

        try {
            signUp_button.setProgress(30);
            if (internetConnection.isNetworkAvailable(SignUpActivity.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        signUp_button.setProgress(30);
                    }
                });
                // Instantiate the RequestQueue.
                Ion.with(SignUpActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.SignUp_Address))
                        .setBodyParameter("phone", phone.trim())
                        .setBodyParameter("email", email.trim().toLowerCase())
                        .setBodyParameter("nationalId", idNumber.trim())
                        .setBodyParameter("username", username.trim())
                        .setBodyParameter("password", password.trim())
                        .setBodyParameter("c_password", password.trim())
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        try {
                                            tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
                                            if (Objects.equals(tokenRequest.getSuccess(), "true")) {
                                                userData.Set_UserToken(SignUpActivity.this,tokenRequest.getToken(),false);
                                                if (userData.Check_UserToken(SignUpActivity.this)) {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            signUp_button.setProgress(100);
                                                        }
                                                    });
                                                    Intent intent = new Intent(SignUpActivity.this, AddCarsActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            }
                                        } catch (Exception e1) {
                                            Error error = gson.fromJson(result.getAsJsonObject("response").getAsJsonObject("error"), Error.class);
                                            if (error.getUsername() != null) {
                                                usernameLayout_signup.setError(error.getUsername().get(0));
                                            }
                                            if (error.getEmail() != null) {
                                                emailLayout_signup.setError(error.getEmail().get(0));
                                            }
                                            if (error.getNationalId() != null) {
                                                idNumberLayout_signup.setError(error.getNationalId().get(0));
                                            }
                                            if (error.getPhone() != null) {
                                                phoneLayout_signup.setError(error.getPhone().get(0));
                                            }
                                            if (error.getPassword() != null) {
                                                passwordLayout_signup.setError(error.getPassword().get(0));
                                            }
                                            if (error.getC_password() != null) {
                                                password2Layout_signup.setError(error.getC_password().get(0));
                                            }
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    signUp_button.setProgress(-1);
                                                }
                                            });
                                            Snackbar snackbar = Snackbar
                                                    .make(mScrollViewContainer, R.string.error_please_check_your_data_again, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "Sign Up Activity 1 : " + error.getMessage());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                signUp_button.setProgress(-1);
                                            }
                                        });
                                        Snackbar snackbar = Snackbar
                                                .make(mScrollViewContainer, R.string.error_there_is_an_error_please_contact_the_admin, Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }
                                }

                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(mScrollViewContainer, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                attemptSignUp();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    signUp_button.setProgress(-1);
                }
            });
            Snackbar snackbar = Snackbar
                    .make(mScrollViewContainer, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), " Exception = " + String.valueOf(e));
        }

    }



}
