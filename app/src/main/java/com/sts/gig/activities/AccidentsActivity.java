package com.sts.gig.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.adapters.AccidentsRecyclerViewAdapter;
import com.sts.gig.classes.Accident;
import com.sts.gig.classes.TokenRequestAccident;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccidentsActivity extends AppCompatActivity implements AccidentsRecyclerViewAdapter.ItemClickListener {

    public static int accident_activity_id=0;
    private static List<Accident> mAccidents;
  //  private static List<Accident_2>mAccidents;
    ConstraintLayout mLayout;
    AccidentsRecyclerViewAdapter adapter;
    @BindView(R.id.iv_logo)
    ImageView mLogo;
    private InternetConnection internetConnection;
    private TokenRequestAccident tokenRequestAccidents;
    private UserData userData;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(AccidentsActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_accidents);
        ButterKnife.bind(this);
        userData = new UserData();
        userData.isLogout(this);
        mLayout = findViewById(R.id.cl_accidents);
        progressBar = findViewById(R.id.pb_accidents);
        internetConnection = new InternetConnection();
        mAccidents = new ArrayList<>();
        loadAccidents();
        // set up the RecyclerView
        recyclerView = findViewById(R.id.rv_accidents);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AccidentsRecyclerViewAdapter(this, mAccidents);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        mLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccidentsActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        accident_activity_id=1;
    }

    @Override
    public void onItemClick(View view, int position) {

        Intent intent = new Intent(AccidentsActivity.this, CompensationActivity.class);

       String damage= adjustDamageArea(adapter.getItem(position).getDamageArea());

        intent.putExtra("Accident_ID", adapter.getItem(position).getId());
        intent.putExtra("Accident_Editable", adapter.getItem(position).getEditable());
       // intent.putExtra("Accident_DamageArea", adapter.getItem(position).getDamageArea());
        intent.putExtra("Accident_DamageArea", damage);

        startActivity(intent);

    }
    private String adjustDamageArea(String damageArea)
    {
        if (damageArea != null && damageArea.length() > 0 && damageArea.charAt(damageArea.length() - 1) == ',')
        {
            damageArea = damageArea.substring(0, damageArea.length() - 1);
        }
        return damageArea;

    }

    void loadAccidents() {
        // data to populate the RecyclerView with
        try {
            if (internetConnection.isNetworkAvailable(AccidentsActivity.this)) {
                progressBar.setVisibility(View.VISIBLE);
                // Instantiate the RequestQueue.
                Ion.with(AccidentsActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.GetCarAccident_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(AccidentsActivity.this))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>()
                        {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {

                               ///////////////
                               ///////////////
                                       Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequestAccidents = gson.fromJson(result.getAsJsonObject("response"), TokenRequestAccident.class);

                                        if (Objects.equals(tokenRequestAccidents.getSuccess(), "true")) {
                                          mAccidents.addAll(tokenRequestAccidents.getAccidents());



                                            adapter.notifyDataSetChanged();
                                            progressBar.setVisibility(View.GONE);
                                        } else {
                                            progressBar.setVisibility(View.GONE);
//                                    //If an error occurs that means end of the list has reached
                                            Snackbar snackbar = Snackbar
                                                    .make(mLayout, R.string.error_downloading_data_from_the_server, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "CarAccident Activity 1 : " + error.getMessage());
                                    }
                                }

                            }
                        });
            } else {
                progressBar.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar
                        .make(mLayout, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadAccidents();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(mLayout, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "CarAccidentActivity Exception = " + String.valueOf(e));
        }
    }
}
