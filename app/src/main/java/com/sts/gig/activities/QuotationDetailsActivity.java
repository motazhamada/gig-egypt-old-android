package com.sts.gig.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;
import com.sts.gig.adapters.*;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.PendingIntent.getActivity;

public class QuotationDetailsActivity extends AppCompatActivity {

    private Context mContext;
    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private View mView;



    private UserData userData;
    ActionProcessButton RequestQuotation;


    @BindView(R.id.iv_quotation_image)
    ImageView mImageImageView;
    @BindView(R.id.tv_quotation_title)
    TextView mTitleTextView;
    @BindView(R.id.tv_quotation_text)
    TextView mTextTextView;
    @BindView(R.id.iv_logo)
    ImageView mLogo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(QuotationDetailsActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_quotation_details);
        ButterKnife.bind(this);
        userData = new UserData();
        userData.isLogout(this);
        mLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuotationDetailsActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        Intent mIntent = getIntent();
        if (mIntent.getStringExtra("imageUrl") != null) {
            Ion.with(this)
                    .load(mIntent.getStringExtra("imageUrl"))
                    .withBitmap()
                    .placeholder(R.drawable.ic_image_black_24dp)
                    .error(R.drawable.ic_broken_image_black_24dp)
                    .intoImageView(mImageImageView);
        }
        if (mIntent.getStringExtra("name") != null) {
            mTitleTextView.setText(mIntent.getStringExtra("name"));
        }
        if (mIntent.getStringExtra("text") != null) {
            mTextTextView.setText(mIntent.getStringExtra("text"));
        }


        RequestQuotation=findViewById(R.id.requestQuotation_2);
        RequestQuotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(LoginActivity.testActivation==-1)
                {
                    Intent intent = new Intent(v.getContext(), SignUpActivity.class);
                    mContext.startActivity(intent);
                }


                else {
                    sentRequest(QuotationRecyclerViewAdapter.selectedQuot);
                }



            }
        });

         RequestQuotation.setMode(ActionProcessButton.Mode.PROGRESS);
         RequestQuotation.setProgress(0);
        this.mContext = QuotationRecyclerViewAdapter.mContext_2;
        this.mView = QuotationRecyclerViewAdapter.mView_2;
    }
    private void sentRequest(final int quotation_id)
    {


        internetConnection = new InternetConnection();
        try {
            if (internetConnection.isNetworkAvailable(mContext)) {
                // Instantiate the RequestQueue.
                Ion.with(mContext)
                        .load(mContext.getString(R.string.Resource_Address) + mContext.getString(R.string.Make_Quotations_Request_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(mContext))
                        .setBodyParameter("quotation_id", String.valueOf(quotation_id))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result)
                            {

                                if (result != null && !Objects.equals(result, "[]"))
                                {

                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);

                                        if (Objects.equals(tokenRequest.getSuccess(), "true"))
                                        {

                                            RequestQuotation.setProgress(100);
                                            RequestQuotation.setText(R.string.success);

                                            Snackbar snackbar = Snackbar
                                                        .make(mView, R.string.request_sent, Snackbar.LENGTH_LONG);
                                            snackbar.show();

                                        }
                                        else
                                            {
                                                RequestQuotation.setProgress(-1);
                                                RequestQuotation.setText(R.string.error);
                                            Snackbar snackbar = Snackbar
                                                    .make(mView, R.string.request_failed, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error)
                                    {
                                        RequestQuotation.setProgress(-1);
                                        RequestQuotation.setText(R.string.error);
                                        Log.e(mContext.getString(R.string.TAG), "Other Services Adapter 1 : " + error.getMessage());
                                    }

                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            RequestQuotation.setMode(ActionProcessButton.Mode.PROGRESS);
                                            RequestQuotation.setProgress(0);
                                            RequestQuotation.setText(R.string.request);
                                        }
                                    }, 2000);


                                }



                            }
                        });
            }
            else
                {
                    RequestQuotation.setProgress(-1);
                    RequestQuotation.setText(R.string.error);
                Snackbar snackbar = Snackbar
                        .make(mView, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sentRequest(QuotationRecyclerViewAdapter.selectedQuot);
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            RequestQuotation.setProgress(-1);
            RequestQuotation.setText(R.string.error);
            Snackbar snackbar = Snackbar
                    .make(mView, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(mContext.getString(R.string.TAG), "Other Services Adapter 2 Exception = " + String.valueOf(e));
        }

    }
}
