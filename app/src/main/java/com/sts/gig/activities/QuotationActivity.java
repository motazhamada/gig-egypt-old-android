package com.sts.gig.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.adapters.QuotationRecyclerViewAdapter;
import com.sts.gig.classes.Quotation;
import com.sts.gig.classes.TokenRequestQuotation;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuotationActivity extends AppCompatActivity {
    
    private static List<Quotation> mQuotations;
    @BindView(R.id.cl_quotation)
    ConstraintLayout mQuotationsConstrainLayout;
    @BindView(R.id.pb_quotation)
    ProgressBar progressBar;
    QuotationRecyclerViewAdapter adapter;
    private InternetConnection internetConnection;
    private TokenRequestQuotation tokenRequestQuotations;
    private UserData userData;
    @BindView(R.id.iv_logo)
     ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(QuotationActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_quotation);
        ButterKnife.bind(this);
        userData = new UserData();
        userData.isLogout(this);
        internetConnection = new InternetConnection();
        mQuotations = new ArrayList<>();
        loadQuotations();
        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rv_quotation);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new QuotationRecyclerViewAdapter(this, mQuotations, mQuotationsConstrainLayout);
        recyclerView.setAdapter(adapter);
        mLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuotationActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

    }

    boolean loadQuotations() {
        // data to populate the RecyclerView with
        try {
            if (internetConnection.isNetworkAvailable(QuotationActivity.this)) {
                progressBar.setVisibility(View.VISIBLE);
                // Instantiate the RequestQueue.
                Ion.with(QuotationActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.Get_Quotations_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(QuotationActivity.this))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result)
                            {
                                if ((result != null && !Objects.equals(result, "[]")))
                                {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequestQuotations = gson.fromJson(result.getAsJsonObject("response"), TokenRequestQuotation.class);
                                        if (Objects.equals(tokenRequestQuotations.getSuccess(), "true")) {
                                            mQuotations.addAll(tokenRequestQuotations.getQuotations());
                                            adapter.notifyDataSetChanged();
                                            progressBar.setVisibility(View.GONE);
                                        }


                                        else
                                            {
                                            progressBar.setVisibility(View.GONE);
//                                    //If an error occurs that means end of the list has reached
                                            Snackbar snackbar = Snackbar
                                                    .make(mQuotationsConstrainLayout, R.string.error_downloading_data_from_the_server, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "CarAccident Activity 1 : " + error.getMessage());
                                    }
                                }

                            }
                        });
            } else {
                progressBar.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar
                        .make(mQuotationsConstrainLayout, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadQuotations();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(mQuotationsConstrainLayout, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "CarAccidentActivity Exception = " + String.valueOf(e));
        }
        return true;
    }

}
