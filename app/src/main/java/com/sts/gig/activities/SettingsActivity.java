package com.sts.gig.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.classes.TokenRequestCarCount;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity {
    @BindView(R.id.iv_logo)
    ImageView mLogo;
    @BindView(R.id.pb_settings)
    ProgressBar mProgressBar;
    @BindView(R.id.cl_settings)
    ConstraintLayout mConstraintLayoutContainer;
    @BindView(R.id.logout_CardRipple_settings)
    MaterialRippleLayout mChangePasswordCardRipple;
    @BindView(R.id.add_car_CardRipple_settings)
    MaterialRippleLayout mAddCarsCardRipple;
    @BindView(R.id.change_password_CardRipple_settings)
    MaterialRippleLayout mLogoutCardRipple;
    private InternetConnection internetConnection;
    private TokenRequestCarCount tokenRequestCarCount;
    private TokenRequest tokenRequest;
    private UserData userData;
    private MaterialRippleLayout change_language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(SettingsActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        userData = new UserData();
        userData.isLogout(this);

        internetConnection = new InternetConnection();

        mLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        mChangePasswordCardRipple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
        mAddCarsCardRipple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckCarsCount();
            }
        });
        mLogoutCardRipple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });


        if(LoginActivity.testActivation==-1) {
            mChangePasswordCardRipple.setVisibility(View.INVISIBLE);
            mAddCarsCardRipple.setVisibility(View.INVISIBLE);

        }

        change_language=findViewById(R.id.change_language_CardRipple_settings);
        change_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Intent intent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                //startActivity(intent);


            }
        });


        change_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LanguageManager languageManager = new LanguageManager(SettingsActivity.this);
                String current_language = getSharedPreferences("language_settings", Context.MODE_PRIVATE).getString("lang", "en");

                if(current_language.equals("en"))
                {
                    languageManager.setLocale(new Locale("ar","EG"));

                }
                else if(current_language.equals("ar"))
                {
                    languageManager.setLocale(new Locale("en","US"));
                }
                else
                    return;

                    languageManager.setLanguage();

                    Intent intent = new Intent(SettingsActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

            }
        });

    }


    private void CheckCarsCount() {
        try {
            if (internetConnection.isNetworkAvailable(SettingsActivity.this)) {
                mProgressBar.setVisibility(View.VISIBLE);
                Ion.with(SettingsActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.GetMyCarsCount_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(SettingsActivity.this))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result.toString(), "[]")) {
                                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                    tokenRequestCarCount = gson.fromJson(result.getAsJsonObject("response"), TokenRequestCarCount.class);
                                    if (Objects.equals(tokenRequestCarCount.getSuccess(), "true")) {
                                        mProgressBar.setVisibility(View.GONE);
                                        int CarsCount = Integer.valueOf(tokenRequestCarCount.getCarCount());
                                        if (CarsCount < 3) {
                                            Intent intent = new Intent(SettingsActivity.this, AddCarsActivity.class);
                                            intent.putExtra("CarsCount", CarsCount);
                                            startActivity(intent);
                                        } else {
                                            Snackbar snackbar = Snackbar
                                                    .make(mConstraintLayoutContainer, R.string.error_sorry_you_can_not_add_more_than_3_cars, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } else {
                                        mProgressBar.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(mConstraintLayoutContainer, tokenRequestCarCount.getError(), Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }
                                }
                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(mConstraintLayoutContainer, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                CheckCarsCount();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            mProgressBar.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(mConstraintLayoutContainer, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "HomeActivity Exception = " + String.valueOf(e));
        }
    }

    private void logout() {
        try {
            if (internetConnection.isNetworkAvailable(SettingsActivity.this)) {
                if (LoginActivity.testActivation == -1)
                {
                    writeToFile("");
                    LoginActivity.testActivation=0;
                    mProgressBar.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    mProgressBar.setVisibility(View.GONE);
                }
                else
                {


                    mProgressBar.setVisibility(View.VISIBLE);
                Ion.with(SettingsActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.UpdateToken_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(SettingsActivity.this))
                        .setBodyParameter("firebaseToken", "1")
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result.toString(), "[]")) {
                                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                    tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
                                    if (Objects.equals(tokenRequest.getSuccess(), "true")) {
                                        userData.Clear_UserData(SettingsActivity.this);
//                                        try {
//                                            FirebaseInstanceId.getInstance().deleteInstanceId();
//                                        } catch (Exception e1) {
//                                            Snackbar snackbar = Snackbar
//                                                    .make(mConstraintLayoutContainer, "there was an error in Deleting your firebase id", Snackbar.LENGTH_LONG);
//                                            snackbar.show();
//                                        }
                                        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                        mProgressBar.setVisibility(View.GONE);
                                    } else {
                                        mProgressBar.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(mConstraintLayoutContainer, R.string.error_there_was_an_error_please_try_again , Snackbar.LENGTH_LONG);
                                        snackbar.show();

                                        Log.d("firebaseError", getString(R.string.error_there_was_an_error_in_deleting_your_firebase_id));
                                    }
                                }
                            }
                        });
                }
            }
            else {
                Snackbar snackbar = Snackbar
                        .make(mConstraintLayoutContainer, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                logout();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            mProgressBar.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(mConstraintLayoutContainer, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "HomeActivity Exception = " + String.valueOf(e));
        }
    }

    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("Config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
