package com.sts.gig.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.dd.processbutton.iml.ActionProcessButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.Brand;
import com.sts.gig.classes.MakeYear;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.classes.TokenRequestBrands;
import com.sts.gig.classes.TokenRequestMakeYear;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class AddCarsActivity extends AppCompatActivity {

    public static final int BRAND_INT_VALUE = 0;
    public static final int MAKE_YEAR_INT_VALUE = 1;
    public static final String PROMPT_VALUE_LIST = "----";
    ConstraintLayout plateNumber_constraintLayout_addCars;
    ActionProcessButton continue_Button_AddCars, addAnotherCar_button_addCars,skip_step_button;
    @BindView(R.id.sv_add_cars)
    ScrollView mScrollViewContainer;
    @BindViews({R.id.spinner_plate_1, R.id.spinner_plate_2, R.id.spinner_plate_3, R.id.spinner_plate_4, R.id.spinner_plate_5, R.id.spinner_plate_6})
    List<Spinner> plate_numbers;
    @BindViews({R.id.spinner_plate_a, R.id.spinner_plate_b, R.id.spinner_plate_c})
    List<Spinner> plate_alphabets;
    @BindView(R.id.spinner_make_year)
    Spinner makeYear_addCars;
    @BindView(R.id.spinner_brand)
    Spinner brand_addCars;
    int mPlateNumberCounter, mPlateAlphabetCounter;
    private EditText model_editText_addCars, chassis_editText_addCars, document_number_editText_addCars;
    private UserData userData;
    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private int mCarsCount = 0;
    private List<String> mBrands, mMakeYears;
    private TokenRequestBrands tokenRequestBrands;
    private TokenRequestMakeYear tokenRequestMakeYear;

    public String textsToString() {
        try {
            mPlateNumberCounter = 0;
            mPlateAlphabetCounter = 0;
            StringBuilder plate = new StringBuilder();
            StringBuilder plate_number = new StringBuilder();
            StringBuilder plate_alphabet = new StringBuilder();
            for (int i = 0; i < plate_numbers.size(); i++) {
                if (!String.valueOf(plate_numbers.get(i).getSelectedItem()).equals("-")) {
                    mPlateNumberCounter++;
                    plate_number.append(String.valueOf(plate_numbers.get(i).getSelectedItem()));
                }
            }
            for (int i = 0; i < plate_alphabets.size(); i++) {
                if (!String.valueOf(plate_alphabets.get(i).getSelectedItem()).equals("-")) {
                    mPlateAlphabetCounter++;
                    plate_alphabet.append(String.valueOf(plate_alphabets.get(i).getSelectedItem()));
                    plate_alphabet.append(" ");
                }
            }
            plate.append(plate_number).append(" ").append(plate_alphabet);
            return plate.toString();
        } catch (Exception error) {
            Log.e(getString(R.string.TAG), "Add Cars Activity 2 : " + error.getMessage());
            return "no";
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(AddCarsActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_add_cars);
        ButterKnife.bind(this);

        internetConnection = new InternetConnection();
        userData = new UserData();
        mBrands = new ArrayList<>();
        mMakeYears = new ArrayList<>();

        mBrands.add(PROMPT_VALUE_LIST);
        mMakeYears.add(PROMPT_VALUE_LIST);

        loadData(BRAND_INT_VALUE);

        plateNumber_constraintLayout_addCars = findViewById(R.id.plateNumber_linearLayout_addCars);

        model_editText_addCars = findViewById(R.id.model_editText_addCars);
        chassis_editText_addCars = findViewById(R.id.chassis_editText_addCars);
        document_number_editText_addCars = findViewById(R.id.document_number_editText_addCars);

        skip_step_button=findViewById(R.id.skip_step);
        skip_step_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                skip_step_button.setProgress(100);
                Intent intent = new Intent(AddCarsActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });

        addAnotherCar_button_addCars = findViewById(R.id.addAnotherCar_button_addCars);
        addAnotherCar_button_addCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptAddCar(addAnotherCar_button_addCars);
            }
        });
        continue_Button_AddCars = findViewById(R.id.continue_button_addCars);
        continue_Button_AddCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptAddCar(continue_Button_AddCars);
            }
        });

        if (getIntent().getIntExtra("CarsCount", 0) == 2) {
            addAnotherCar_button_addCars.setVisibility(View.GONE);
        } else {
            mCarsCount = getIntent().getIntExtra("CarsCount", 0);
        }

    }


    void attemptAddCar(ActionProcessButton button) {

        // Reset errors.

        model_editText_addCars.setError(null);
        chassis_editText_addCars.setError(null);
        document_number_editText_addCars.setError(null);

        //makeYear_addCars.setError(null);


        // Store values at the time of the signup attempt.
//        String brand = brand_editText_addCars.getText().toString();
        String brand = brand_addCars.getSelectedItem().toString();
        String model = model_editText_addCars.getText().toString();

        ////////////////
        String chassis = chassis_editText_addCars.getText().toString();

        if ( chassis.length() <3)
        {
            chassis_editText_addCars.setError("please provide your mobile number");

        }
        //////////////////
        String documentNumber = document_number_editText_addCars.getText().toString();
        String makeYear = makeYear_addCars.getSelectedItem().toString();
        String plateNumber = textsToString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(brand) || brand_addCars.equals(PROMPT_VALUE_LIST)) {
            focusView = brand_addCars;
            cancel = true;
        }
        if (TextUtils.isEmpty(model)) {
            model_editText_addCars.setError(getString(R.string.error_field_required));
            focusView = model_editText_addCars;
            cancel = true;
        }
        if ( chassis.isEmpty() || chassis.length() < 3) {
            chassis_editText_addCars.setError("please provide a valid number");
            focusView = chassis_editText_addCars;
            cancel = true;
        }
//       if (TextUtils.isEmpty(documentNumber)) {
//            document_number_editText_addCars.setError(getString(R.string.error_field_required));
//            focusView = document_number_editText_addCars;
//            cancel = true;
//        }
        if (TextUtils.isEmpty(makeYear) || makeYear.equals(PROMPT_VALUE_LIST)) {
            focusView = makeYear_addCars;
            cancel = true;
        }
        if (TextUtils.isEmpty(plateNumber)) {
            focusView = plateNumber_constraintLayout_addCars;
            Snackbar snackbar = Snackbar
                    .make(mScrollViewContainer, getString(R.string.error_field_required), Snackbar.LENGTH_LONG);
            snackbar.show();
            cancel = true;
        }
        if (mPlateNumberCounter > 4 && mPlateAlphabetCounter > 0 || mPlateNumberCounter < 2 && mPlateAlphabetCounter < 2) {
            focusView = plateNumber_constraintLayout_addCars;
            Snackbar snackbar = Snackbar
                    .make(mScrollViewContainer, R.string.error_this_plate_is_not_acceptable, Snackbar.LENGTH_LONG);
            snackbar.show();
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt signup and focus the first
            // form field with an error.
            focusView.requestFocus();
            button.setProgress(-1);

        } else {
            button.setProgress(0);
            AddCar(button, brand, model, chassis, documentNumber,  makeYear);

        }

    }

    protected void AddCar(final ActionProcessButton button, final String brand, final String model, final String chassis, final String documentNumber,  final String makeYear) {

        try {
            if (internetConnection.isNetworkAvailable(AddCarsActivity.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        button.setProgress(30);
                    }
                });
                // Instantiate the RequestQueue.
                Ion.with(AddCarsActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.AddCar_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(AddCarsActivity.this))
                        .setBodyParameter("brand", brand.trim())
                        .setBodyParameter("model", model.trim())
                        .setBodyParameter("chassisNumber", chassis.trim())
                        .setBodyParameter("documentId", documentNumber.trim())
                        .setBodyParameter("plateNumber", textsToString())
                        .setBodyParameter("makeYear", makeYear.trim())
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
                                        if (Objects.equals(tokenRequest.getSuccess(), "true")) {
                                            mCarsCount++;
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    button.setProgress(100);
                                                }
                                            });
                                            Intent intent;
                                            if (Objects.equals(button, continue_Button_AddCars)) {
                                                if (userData.isLogin(AddCarsActivity.this)) {
                                                    intent = new Intent(AddCarsActivity.this, HomeActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                } else {
                                                    intent = new Intent(AddCarsActivity.this, LoginActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    userData.Clear_UserData(AddCarsActivity.this);
                                                }
                                            } else {
                                                intent = new Intent(AddCarsActivity.this, AddCarsActivity.class);
                                                intent.putExtra("CarsCount", mCarsCount);
                                            }
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    button.setProgress(-1);
                                                }
                                            });

                                            Snackbar snackbar = Snackbar
                                                    .make(mScrollViewContainer, tokenRequest.getError(), Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "Add Cars Activity 1 : " + error.getMessage());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                button.setProgress(-1);
                                            }
                                        });
                                    }
                                }
                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(mScrollViewContainer, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                attemptAddCar(button);
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    button.setProgress(-1);
                }
            });
            Snackbar snackbar = Snackbar
                    .make(mScrollViewContainer, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), " Exception = " + String.valueOf(e));
        }
    }

    void loadData(final int mType) {
        // data to populate the RecyclerView with

        try {
            if (internetConnection.isNetworkAvailable(AddCarsActivity.this)) {
                // Instantiate the RequestQueue.
                String mUrl;
                if (mType == BRAND_INT_VALUE) {
                    mUrl = getString(R.string.Resource_Address) + getString(R.string.GetCarBrand_Address);
                } else if (mType == MAKE_YEAR_INT_VALUE){
                    mUrl = getString(R.string.Resource_Address) + getString(R.string.GetMakeYear_Address);
                }
                else
                {
                    startActivity(new Intent(AddCarsActivity.this, SettingsActivity.class));
                    finish();
                    return;
                }
                Ion.with(AddCarsActivity.this)
                        .load(mUrl)
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(AddCarsActivity.this))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        if (mType == BRAND_INT_VALUE) {
                                            tokenRequestBrands = gson.fromJson(result.getAsJsonObject("response"), TokenRequestBrands.class);
                                        } else {
                                            tokenRequestMakeYear = gson.fromJson(result.getAsJsonObject("response"), TokenRequestMakeYear.class);
                                        }
                                        if (Objects.equals(tokenRequestBrands.getSuccess(), "true")) {
                                            if (mType == BRAND_INT_VALUE) {
                                                for (Brand b : tokenRequestBrands.getBrands()) {
                                                    mBrands.add(b.getBrand());
                                                }
                                                ArrayAdapter<String> adapter = new ArrayAdapter<>(AddCarsActivity.this, android.R.layout.simple_spinner_item, mBrands);
                                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                brand_addCars.setAdapter(adapter);

                                                loadData(MAKE_YEAR_INT_VALUE);

                                            } else {


                                                for (MakeYear m : tokenRequestMakeYear.getMakeYears()) {
                                                    mMakeYears.add(m.getYear());
                                                }


                                                ArrayAdapter<String> adapter = new ArrayAdapter<>(AddCarsActivity.this, android.R.layout.simple_spinner_item, mMakeYears);
                                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                makeYear_addCars.setAdapter(adapter);
                                            }
                                        } else {
//                                    //If an error occurs that means end of the list has reached
                                            Snackbar snackbar = Snackbar
                                                    .make(mScrollViewContainer, R.string.error_downloading_data_from_the_server, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "CarAccident Activity 1 : " + error.getMessage());
                                    }
                                }
                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(mScrollViewContainer, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadData(mType);
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {

            Snackbar snackbar = Snackbar
                    .make(mScrollViewContainer, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "CarAccidentActivity Exception = " + String.valueOf(e));
        }
    }


}
