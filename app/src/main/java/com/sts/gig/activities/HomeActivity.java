package com.sts.gig.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.balysv.materialripple.MaterialRippleLayout;
import com.sts.gig.R;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity
{
   // public  static int testActivation;

    private ProgressBar progressBar_home;
    @BindView(R.id.cl_home)
    ConstraintLayout mConstraintLayoutContainer;
    private UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(HomeActivity.this);
        languageManager.getLanguage();

        try {

            setContentView(R.layout.activity_home);

            ButterKnife.bind(this);
            userData = new UserData();
            userData.isLogout(this);

            progressBar_home = findViewById(R.id.progressBar_home);

            MaterialRippleLayout quotationRequestCardRipple = findViewById(R.id.quotationRequestCardRipple_Home);
            MaterialRippleLayout otherServicesCardRipple = findViewById(R.id.otherServicesCardRipple_Home);
            MaterialRippleLayout compensationCardRipple = findViewById(R.id.compensationCardRipple_Home);
            MaterialRippleLayout reportAccidentCardRipple = findViewById(R.id.reportAccidentCardRipple_Home);
            MaterialRippleLayout messagesCardRipple = findViewById(R.id.messagesCardRipple_Home);
            MaterialRippleLayout logoutCardRipple = findViewById(R.id.settingsCardRipple_Home);


            if(LoginActivity.testActivation==0) {
                otherServicesCardRipple.setVisibility(View.INVISIBLE);
                compensationCardRipple.setVisibility(View.INVISIBLE);
                reportAccidentCardRipple.setVisibility(View.INVISIBLE);
                messagesCardRipple.setVisibility(View.INVISIBLE);
            }
            else if(LoginActivity.testActivation==-1) {
                otherServicesCardRipple.setVisibility(View.INVISIBLE);
                compensationCardRipple.setVisibility(View.INVISIBLE);
                reportAccidentCardRipple.setVisibility(View.INVISIBLE);
                messagesCardRipple.setVisibility(View.INVISIBLE);
            }



            quotationRequestCardRipple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                        Intent intent = new Intent(HomeActivity.this, QuotationActivity.class);
                        startActivity(intent);


                }
            });
            otherServicesCardRipple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, NewsActivity.class);
                    startActivity(intent);
                }
            });
            compensationCardRipple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this,AccidentsActivity.class);
                    startActivity(intent);
                }
            });
            reportAccidentCardRipple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, CarsActivity.class);
                    startActivity(intent);
                }
            });
            messagesCardRipple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, MessagesActivity.class);
                    startActivity(intent);
                }
            });
            logoutCardRipple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
                    startActivity(intent);
                }
            });

        }
        catch (Exception e)
        {
            Log.e(getString(R.string.TAG), "Error: " + e.getMessage());
        }
    }
    /*
    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
*/

}
