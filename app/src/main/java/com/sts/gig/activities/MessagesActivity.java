package com.sts.gig.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.adapters.ChatRecyclerViewAdapter;
import com.sts.gig.classes.Chat;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.classes.TokenRequestChat;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesActivity extends AppCompatActivity {

    @BindView(R.id.cl_messages)
    ConstraintLayout mLayout;
    @BindView(R.id.pb_chat)
    ProgressBar progressBar;
    @BindView(R.id.tb_messages)
    Toolbar toolbar;
    @BindView(R.id.rv_messages)
    RecyclerView recyclerView;
    @BindView(R.id.et_message)
    EditText mMessageEditText;
    @BindView(R.id.btn_send)
    ImageButton mSendButton;
    ChatRecyclerViewAdapter adapter;
    private Chat mChat;
    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private TokenRequestChat tokenRequestChat;
    private UserData userData;
    private Handler handler;
    private Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(MessagesActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);

        userData = new UserData();
        userData.isLogout(this);

        internetConnection = new InternetConnection();
        mChat = new Chat();
        loadChat();
        // set up the RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mMessageEditText.getText().toString().trim().equals("")){
                    sendMessage(mMessageEditText.getText().toString());
                }
            }
        });

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                loadChat();
                handler.postDelayed(this, 3000);
            }
        };
        handler.postDelayed(runnable, 3000);
    }

    private void sendMessage(final String mMessageContent) {
        try {
            if (internetConnection.isNetworkAvailable(MessagesActivity.this)) {
//                progressBar.setVisibility(View.VISIBLE);
                // Instantiate the RequestQueue.
                Ion.with(MessagesActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.Push_Message_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(MessagesActivity.this))
                        .setBodyParameter("chat_id",mChat.getId())
                        .setBodyParameter("content",mMessageContent)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
                                        if (Objects.equals(tokenRequestChat.getSuccess(), "true")) {
                                            loadChat();
                                            mMessageEditText.setText("");
////                                            mCars.addAll(tokenRequestChat.getCars());
////                                            adapter.notifyDataSetChanged();
//                                            adapter = new ChatRecyclerViewAdapter(MessagesActivity.this, mChat);
//                                            recyclerView.setAdapter(adapter);
//                                            progressBar.setVisibility(View.GONE);
                                        } else {
//                                            progressBar.setVisibility(View.GONE);
//                                    //If an error occurs that means end of the list has reached
                                            Snackbar snackbar = Snackbar
                                                    .make(mLayout, R.string.error_sorry_there_was_an_error_with_sending_the_message, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "CarAccident Activity 1 : " + error.getMessage());
                                    }
                                }

                            }
                        });
            } else {
//                progressBar.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar
                        .make(mLayout, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sendMessage(mMessageContent);
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
//            progressBar.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(mLayout, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "CarAccidentActivity Exception = " + String.valueOf(e));
        }

    }


    void loadChat() {
        // data to populate the RecyclerView wit
        try {
            if (internetConnection.isNetworkAvailable(MessagesActivity.this)) {
//                progressBar.setVisibility(View.VISIBLE);
                // Instantiate the RequestQueue.
                Ion.with(MessagesActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.GetChat_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(MessagesActivity.this))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequestChat = gson.fromJson(result.getAsJsonObject("response"), TokenRequestChat.class);
                                        if (Objects.equals(tokenRequestChat.getSuccess(), "true")) {
                                            if(adapter!=null){
                                                if (tokenRequestChat.getChats().getMessages().size() == adapter.getItemCount()) {
                                                    return;
                                                }
                                            }
                                            progressBar.setVisibility(View.VISIBLE);
                                            mChat = tokenRequestChat.getChats();
                                                adapter = new ChatRecyclerViewAdapter(MessagesActivity.this, mChat);
                                                recyclerView.setAdapter(adapter);
                                                recyclerView.smoothScrollToPosition(adapter.getItemCount());


                                            progressBar.setVisibility(View.GONE);
                                        } else {
//                                            progressBar.setVisibility(View.GONE);
//                                    //If an error occurs that means end of the list has reached
                                            Snackbar snackbar = Snackbar
                                                    .make(mLayout, R.string.error_downloading_data_from_the_server, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "CarAccident Activity 1 : " + error.getMessage());
                                    }
                                }

                            }
                        });
            } else {
//                progressBar.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar
                        .make(mLayout, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadChat();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
//            progressBar.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(mLayout, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "CarAccidentActivity Exception = " + String.valueOf(e));
        }
    }
}
