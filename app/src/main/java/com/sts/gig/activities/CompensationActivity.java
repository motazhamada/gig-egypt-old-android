package com.sts.gig.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.TokenRequestInsurance;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompensationActivity extends AppCompatActivity {
    ConstraintLayout constrainLayout;
    ProgressBar progressBar_compensation;
    TextView dateOfPreviewBefore_textview, dateOfReport_textview, dateOfCheckDelivery_textview, dateOfPreviewAfter_textview;
    ImageView previewBefore_imageview, report_imageview, checkDelivery_imageview, previewAfter_imageview;
    private String mAccident_ID;
    //private int mAccident_ID;
    private String mAccident_Editable, mAccident_DamageArea;
    private InternetConnection internetConnection;
    private TokenRequestInsurance tokenRequestInsurance;
    private UserData userData;

    private boolean check_report;
    private boolean check_report_2;
    @BindView(R.id.iv_logo)
     ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(CompensationActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_compensation);
        ButterKnife.bind(this);
        initialize();
        getInsuranceData();

        check_report=false;
        check_report_2=false;
    }

    private void initialize() {
        userData = new UserData();
        userData.isLogout(this);
        internetConnection = new InternetConnection();

        Intent intent = getIntent();
        mAccident_ID = intent.getStringExtra("Accident_ID");
        mAccident_Editable = intent.getStringExtra("Accident_Editable");
        mAccident_DamageArea = intent.getStringExtra("Accident_DamageArea");


        constrainLayout = findViewById(R.id.constrainLayout_compensation);

        progressBar_compensation = findViewById(R.id.pb_settings);

        MaterialRippleLayout reportAccidentCardRipple_compensation = findViewById(R.id.reportAccidentCardRipple_compensation);
        reportAccidentCardRipple_compensation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(check_report==false) {
                    Intent intent = new Intent(CompensationActivity.this, CarAccidentActivity.class);
                    intent.putExtra("Accident_ID", mAccident_ID);
                    intent.putExtra("Accident_Editable", mAccident_Editable);
                    intent.putExtra("Accident_DamageArea", mAccident_DamageArea);
                    startActivity(intent);
                }
                else
                {

                }
            }
        });
        mLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CompensationActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        dateOfPreviewBefore_textview = findViewById(R.id.dateOfPreviewBefore_textview);
        previewBefore_imageview = findViewById(R.id.previewBefore_imageview);

        dateOfReport_textview = findViewById(R.id.dateOfReport_textview);
        report_imageview = findViewById(R.id.report_imageview);

        dateOfCheckDelivery_textview = findViewById(R.id.dateOfCheckDelivery_textview);
        checkDelivery_imageview = findViewById(R.id.checkDelivery_imageview);

        dateOfPreviewAfter_textview = findViewById(R.id.dateOfPreviewAfter_textview);
        previewAfter_imageview = findViewById(R.id.previewAfter_imageview);
    }

    private void getInsuranceData() {
        try {
            if (internetConnection.isNetworkAvailable(CompensationActivity.this)) {
                // Instantiate the RequestQueue.
                Ion.with(CompensationActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.GetInsuranceRequest_Address)+"?accident_id="+mAccident_ID)
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(CompensationActivity.this))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequestInsurance = gson.fromJson(result.getAsJsonObject("response"), TokenRequestInsurance.class);
                                        if (Objects.equals(tokenRequestInsurance.getSuccess(), "true")) {
                                            dateOfPreviewBefore_textview.setText(tokenRequestInsurance.getInsurance().get(0).getPreviewBeforeDate());
                                            dateOfPreviewAfter_textview.setText(tokenRequestInsurance.getInsurance().get(0).getPreviewAfterDate());
                                            dateOfCheckDelivery_textview.setText(tokenRequestInsurance.getInsurance().get(0).getCheckDate());
                                            dateOfReport_textview.setText(tokenRequestInsurance.getInsurance().get(0).getReportDate());

                                            if (tokenRequestInsurance.getInsurance().get(0).getPreviewBeforeReform().trim().equals("0")) {
                                                previewBefore_imageview.setImageResource(R.drawable.ic_close_24dp);
                                                report_imageview.setImageResource(R.drawable.ic_close_24dp);
                                                check_report=false;
                                            } else {
                                                previewBefore_imageview.setImageResource(R.drawable.ic_check_24dp);
                                                report_imageview.setImageResource(R.drawable.ic_check_24dp);
                                                check_report=true;
                                            }
                                            /*
                                            if (tokenRequestInsurance.getInsurance().get(0).getPreviewAfterReform().trim().equals("0")) {
                                                previewAfter_imageview.setImageResource(R.drawable.ic_close_24dp);
                                                checkDelivery_imageview.setImageResource(R.drawable.ic_check_24dp);
                                                check_report_2=true;
                                            }
                                            else
                                                {
                                                previewAfter_imageview.setImageResource(R.drawable.ic_check_24dp);
                                                    checkDelivery_imageview.setImageResource(R.drawable.ic_close_24dp);
                                                    check_report_2=false;
                                            }
                                            */

                                            if (tokenRequestInsurance.getInsurance().get(0).getCheckDelivery().trim().equals("0")) {
                                                checkDelivery_imageview.setImageResource(R.drawable.ic_close_24dp);
                                            } else {
                                                checkDelivery_imageview.setImageResource(R.drawable.ic_check_24dp);
                                            }
                                            if (tokenRequestInsurance.getInsurance().get(0).getPreviewAfterReform().trim().equals("0")) {
                                                previewAfter_imageview.setImageResource(R.drawable.ic_close_24dp);
                                            } else {
                                                previewAfter_imageview.setImageResource(R.drawable.ic_check_24dp);
                                            }


//                                            if (tokenRequestInsurance.getInsurance().get(0).getReportAccident().trim().equals("0")) {
//                                                check_report = false;
//                                                report_imageview.setImageResource(R.drawable.ic_close_24dp);
//                                            } else {
//                                                check_report = true;
//                                                report_imageview.setImageResource(R.drawable.ic_check_24dp);
//                                            }


                                            progressBar_compensation.setVisibility(View.GONE);
                                        } else {
                                            progressBar_compensation.setVisibility(View.GONE);
                                            Snackbar snackbar = Snackbar
                                                    .make(constrainLayout, tokenRequestInsurance.getError(), Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception e1) {
                                        progressBar_compensation.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(constrainLayout, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }

                                }

                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(constrainLayout, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getInsuranceData();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e2) {
            Snackbar snackbar = Snackbar
                    .make(constrainLayout, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "Compensations Exception = " + String.valueOf(e2));
        }
    }
}
