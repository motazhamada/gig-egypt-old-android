package com.sts.gig.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.adapters.CarsRecyclerViewAdapter;
import com.sts.gig.classes.Car;
import com.sts.gig.classes.TokenRequestCars;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarsActivity extends AppCompatActivity implements CarsRecyclerViewAdapter.ItemClickListener {

    public static int Cars_actvity_id=0;
    private static List<Car> mCars;
    ConstraintLayout mLayout;
    CarsRecyclerViewAdapter adapter;
    private InternetConnection internetConnection;
    private TokenRequestCars tokenRequestCars;
    private UserData userData;
    private ProgressBar progressBar;
    @BindView(R.id.iv_logo)
     ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(CarsActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_cars);
        ButterKnife.bind(this);
        internetConnection = new InternetConnection();
        userData = new UserData();
        userData.isLogout(this);

        mLayout = findViewById(R.id.cl_cars);
        progressBar = findViewById(R.id.pb_cars);
        mCars = new ArrayList<>();
        loadMyCars();
        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rv_cars);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CarsRecyclerViewAdapter(this, mCars);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        mLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CarsActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        Cars_actvity_id=1;
    }

    @Override
    public void onItemClick(View view, int position) {

        Intent intent = new Intent(CarsActivity.this, CarAccidentActivity.class);

        intent.putExtra("Car_ID", adapter.getItem(position).getCar_ID());

        startActivity(intent);

        finish();
    }


    void loadMyCars() {
        // data to populate the RecyclerView with
        try {

            if (internetConnection.isNetworkAvailable(CarsActivity.this)) {
                progressBar.setVisibility(View.VISIBLE);
                // Instantiate the RequestQueue.
                Ion.with(CarsActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.GetMyCars_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(CarsActivity.this))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequestCars = gson.fromJson(result.getAsJsonObject("response"), TokenRequestCars.class);
                                        if (Objects.equals(tokenRequestCars.getSuccess(), "true")) {
                                            mCars.addAll(tokenRequestCars.getCars());
                                            adapter.notifyDataSetChanged();
                                            progressBar.setVisibility(View.GONE);
                                       } else {
                                            progressBar.setVisibility(View.GONE);
//                                    //If an error occurs that means end of the list has reached
                                            Snackbar snackbar = Snackbar
                                                    .make(mLayout, R.string.error_downloading_data_from_the_server, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    }
                                    catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "CarAccident Activity 1 : " + error.getMessage());
                                    }
                                }

                            }
                        });
            } else {
                progressBar.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar
                        .make(mLayout, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadMyCars();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(mLayout, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "CarAccidentActivity Exception = " + String.valueOf(e));
        }
    }
}
