package com.sts.gig.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;

import com.dd.processbutton.iml.ActionProcessButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.Error;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordActivity extends AppCompatActivity {
    @BindView(R.id.sv_change_password)
    ScrollView mScrollViewContainer;
    @BindView(R.id.oldpasswordLayout)
    TextInputLayout mOldPasswordTextInputLayout;
    @BindView(R.id.oldPassword)
    EditText mOldPasswordEditText;
    @BindView(R.id.passwordLayout)
    TextInputLayout mPasswordTextInputLayout;
    @BindView(R.id.password)
    EditText mPasswordEditText;
    @BindView(R.id.password2Layout)
    TextInputLayout mPassword2TextInputLayout;
    @BindView(R.id.password2)
    EditText mPassword2EditText;
    @BindView(R.id.btn_change_password)
    ActionProcessButton mChangePasswordActionProcessButton;

    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguageManager languageManager = new LanguageManager(ChangePasswordActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        internetConnection = new InternetConnection();

        userData = new UserData();
        userData.isLogout(this);
        mChangePasswordActionProcessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptChangePassword();
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
        mChangePasswordActionProcessButton.setMode(ActionProcessButton.Mode.PROGRESS);
        mChangePasswordActionProcessButton.setProgress(0);
    }

    private void attemptChangePassword() {
        // Reset errors.
        mOldPasswordTextInputLayout.setError(null);
        mPasswordTextInputLayout.setError(null);
        mPassword2TextInputLayout.setError(null);

        // Store values at the time of the signup attempt.
        String oldPassword = mOldPasswordEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();
        String password2 = mPassword2EditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password2, if the user entered one.
        if (!Objects.equals(password, password2)) {
            mPassword2TextInputLayout.setError(getString(R.string.error_mismatch_passwords));
            focusView = mPassword2TextInputLayout;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordTextInputLayout.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordTextInputLayout;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt signup and focus the first
            // form field with an error.
            focusView.requestFocus();
            mChangePasswordActionProcessButton.setProgress(-1);
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user signup attempt.
//            showProgress(true);
            mChangePasswordActionProcessButton.setProgress(0);

            ChangePassword(oldPassword, password);
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 2;
    }

    protected void ChangePassword(final String oldPassword, final String password) {

        try {
            mChangePasswordActionProcessButton.setProgress(30);
            if (internetConnection.isNetworkAvailable(ChangePasswordActivity.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mChangePasswordActionProcessButton.setProgress(30);
                    }
                });
                // Instantiate the RequestQueue.
                Ion.with(ChangePasswordActivity.this)
                        .load(getString(R.string.Resource_Address) + getString(R.string.ChangePassword_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(ChangePasswordActivity.this))
                        .setBodyParameter("password", oldPassword.trim())
                        .setBodyParameter("newPassword", password.trim())
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result.toString(), "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        try {
                                            tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
                                            if (Objects.equals(tokenRequest.getSuccess(), "true")) {
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mChangePasswordActionProcessButton.setProgress(100);
                                                    }
                                                });
                                                Intent intent = new Intent(ChangePasswordActivity.this, HomeActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        } catch (Exception e1) {
                                            Error error = gson.fromJson(result.getAsJsonObject("response").getAsJsonObject("error"), Error.class);
                                            if (error.getPassword() != null) {
                                                mOldPasswordTextInputLayout.setError(error.getPassword().get(0));
                                            }
                                            if (error.getNewPassword() != null) {
                                                mPasswordTextInputLayout.setError(error.getC_password().get(0));
                                            }
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mChangePasswordActionProcessButton.setProgress(-1);
                                                }
                                            });
                                            Snackbar snackbar = Snackbar
                                                    .make(mScrollViewContainer, R.string.error_please_check_your_data_again, Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "Sign Up Activity 1 : " + error.getMessage());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mChangePasswordActionProcessButton.setProgress(-1);
                                            }
                                        });
                                        Snackbar snackbar = Snackbar
                                                .make(mScrollViewContainer, R.string.error_there_is_an_error_please_contact_the_admin, Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }
                                }

                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(mScrollViewContainer, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                attemptChangePassword();
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mChangePasswordActionProcessButton.setProgress(-1);
                }
            });
            Snackbar snackbar = Snackbar
                    .make(mScrollViewContainer, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), " Exception = " + String.valueOf(e));
        }

    }

}
