package com.sts.gig.activities;

import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via phone/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    public static int testActivation = 0;
    public String filePath = "";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mPhoneNumberView;
    private TextInputLayout mPhoneNumberViewLayout;
    private EditText mPasswordView;
    private TextView continue_asGuest;
    private LinearLayout Phone;
    private TextInputLayout mPasswordViewLayout;
    private ActionProcessButton mSignInButton, mSignUpButton;

    private ScrollView mScrollViewContainer;
    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private UserData userData;
    private String deviceToken,phone_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userData = new UserData();

        if (userData.isLogin(this)) {
            if (userData.Check_UserToken(this)) {

                try {
                    checkLogin();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //read the textFile
                try {
                    readFromFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else {
                try {
                    checkLogin();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //read the textFile
                try {
                    readFromFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        LanguageManager languageManager = new LanguageManager(LoginActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_login);
        internetConnection = new InternetConnection();

        mScrollViewContainer = findViewById(R.id.sv_login);

        mPhoneNumberView = findViewById(R.id.phoneNumber_login);
        populateAutoComplete();
        mPhoneNumberViewLayout = findViewById(R.id.phoneNumberLayout_login);
        mPasswordViewLayout = findViewById(R.id.passwordLayout_login);
        mPasswordView = findViewById(R.id.password_login);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mSignInButton = findViewById(R.id.signIn_button_login);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignInButton.setMode(ActionProcessButton.Mode.ENDLESS);
                attemptLogin();

            }
        });
        mSignUpButton = findViewById(R.id.signUp_button_login);
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mSignUpButton.setMode(ActionProcessButton.Mode.ENDLESS);
//                mSignUpButton.setProgress(0);
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
//                mSignUpButton.setProgress(100);
            }

        });

        continue_asGuest = findViewById(R.id.contiue_as_a_guest);
        continue_asGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                writeToFile("0");
                try {
                    checkLogin();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //read the textFile
                try {
                    readFromFile_2();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
                attemptLogin_2();
            }

        });
        phone_number = getString(R.string.customer_service_phone_number);
        Phone=findViewById(R.id.call);
        Phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone_number)));

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    public void checkLogin() throws IOException {
        String FILENAME="Config.txt";

        File file = new File(this.getFilesDir(),FILENAME);
        filePath=file.toString();
        if(!file.exists())
        {
            file.createNewFile();
            // write code for saving data to the file

        }
    }
    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("Config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    private void readFromFile() throws IOException {
        String FILENAME=filePath;
        List<String> mLines = new ArrayList<>();
       // AssetManager am = this.getAssets();
        //InputStream is = am.open(FILENAME);
        //BufferedReader reader = new BufferedReader(new InputStreamReader());
        FileInputStream fileInputStream = new FileInputStream (new File(filePath));
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String line;
        while ((line = bufferedReader.readLine()) != null)
        {
            mLines.add(line);
            if(mLines.get(0).matches("1"))
            {
                LoginActivity.testActivation=1;
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
            else if(mLines.get(0).matches("0"))

            {
                LoginActivity.testActivation=0;
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
            else if(mLines.get(0).matches("-1"))

            {
                LoginActivity.testActivation=-1;
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
    private void readFromFile_2() throws IOException {
        String FILENAME=filePath;
        List<String> mLines = new ArrayList<>();
        // AssetManager am = this.getAssets();
        //InputStream is = am.open(FILENAME);
        //BufferedReader reader = new BufferedReader(new InputStreamReader());
        FileInputStream fileInputStream = new FileInputStream (new File(filePath));
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String line;
        while ((line = bufferedReader.readLine()) != null)
        {
            mLines.add(line);
            if(mLines.get(0).matches("1"))
            {
                LoginActivity.testActivation=1;
                //Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                //startActivity(intent);
                //finish();
            }
            else

            {
                LoginActivity.testActivation=0;
               // Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
               // startActivity(intent);
              //  finish();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSignInButton.setMode(ActionProcessButton.Mode.PROGRESS);
        mSignInButton.setProgress(0);
        mSignUpButton.setMode(ActionProcessButton.Mode.PROGRESS);
        mSignUpButton.setProgress(0);

    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mPhoneNumberView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            mSignInButton.setProgress(-1);
            return;
        }

        // Reset errors.
        mPhoneNumberViewLayout.setError(null);
        mPasswordViewLayout.setError(null);

        // Store values at the time of the login attempt.
        String phoneNumber = mPhoneNumberView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordViewLayout.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberViewLayout.setError(getString(R.string.error_field_required));
            focusView = mPhoneNumberView;
            cancel = true;
        } else if (!isPhoneNumberValid(phoneNumber)) {
            mPhoneNumberViewLayout.setError(getString(R.string.error_invalid_phone_number));
            focusView = mPhoneNumberView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            mSignInButton.setProgress(-1);
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
//            showProgress(true);
            mSignInButton.setProgress(0);
            mAuthTask = new UserLoginTask(phoneNumber, password);
            mAuthTask.execute((Void) null);
        }
    }
    private void attemptLogin_2() {
        if (mAuthTask != null) {

            return;
        }

        // Reset errors.
        mPhoneNumberViewLayout.setError(null);
        mPasswordViewLayout.setError(null);

        // Store values at the time of the login attempt.
        String phoneNumber ="12345678901";
        String password = "123456";


            mAuthTask = new UserLoginTask(phoneNumber, password);
            mAuthTask.execute((Void) null);

    }
    private boolean isPhoneNumberValid(String phoneNumber) {

        return phoneNumber.length() > 10;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 2;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> phoneNumbers = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            phoneNumbers.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addPhoneNumbersToAutoComplete(phoneNumbers);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addPhoneNumbersToAutoComplete(List<String> phoneNumbersAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, phoneNumbersAddressCollection);

        mPhoneNumberView.setAdapter(adapter);
    }
/*
    public void OnClick(View view)
    {
        writeToFile("0");
        try {
            checkLogin();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //read the textFile
        try {
            readFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

      // Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
      //startActivity(intent);
      // finish();
    }
*/
    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
//        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mPhoneNumber;
        private final String mPassword;


        UserLoginTask(String phoneNumber, String password) {
            mPhoneNumber = phoneNumber;
            mPassword = password;
        }

        @Override
        protected void onPreExecute() {
            mSignInButton.setProgress(0);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if (internetConnection.isNetworkAvailable(LoginActivity.this)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSignInButton.setProgress(30);
                        }
                    });

                    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                        @Override
                        public void onSuccess(InstanceIdResult instanceIdResult) {
                            deviceToken = instanceIdResult.getToken();

                        }
                    });


                    // Instantiate the RequestQueue.
                    Ion.with(LoginActivity.this)
                            .load(getString(R.string.Resource_Address) + getString(R.string.Login_Address))
                            .setBodyParameter("phone", mPhoneNumber.trim())
                            .setBodyParameter("password", mPassword)
                            .setBodyParameter("firebaseToken", deviceToken)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {

                                    if (result != null && !Objects.equals(result, "[]")) {
                                        try {
                                            Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                            tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);

                                            String value=tokenRequest.getError();
                                            if (value==null)
                                            {
                                                testActivation = 1;
                                                writeToFile("1");
                                            }

                                            else if (value.matches("Account has not been activated yet") && mPassword.matches("123456") && mPhoneNumber.matches("12345678901"))
                                            {
                                                testActivation = -1;
                                                writeToFile("-1");
                                                tokenRequest.setSuccess("true");
                                            }
                                            else if (value.matches("Account has not been activated yet"))
                                            {
                                                testActivation = 0;
                                                writeToFile("0");
                                            }



                                            if (Objects.equals(tokenRequest.getSuccess(), "true"))
                                            {
                                                userData.Set_UserToken(LoginActivity.this, tokenRequest.getToken(), true);
                                                if (userData.Check_UserToken(LoginActivity.this)==false && mPassword=="0" && mPhoneNumber=="0")
                                                {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            mSignInButton.setProgress(100);
                                                        }
                                                    });
                                                    sendNotification();
                                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                                else if (userData.Check_UserToken(LoginActivity.this))
                                                {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            mSignInButton.setProgress(100);
                                                        }
                                                    });
                                                    sendNotification();
                                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }

                                            }
                                            else {

                                                //mPasswordViewLayout.setError(getString(R.string.error_incorrect_password));
                                                mPasswordView.requestFocus();

                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mSignInButton.setProgress(-1);
                                                    }
                                                });

                                                Snackbar snackbar = Snackbar
                                                        .make(mScrollViewContainer, tokenRequest.getError(), Snackbar.LENGTH_LONG);
                                                snackbar.show();
                                            }
                                        } catch (Exception error) {
                                            Log.e(getString(R.string.TAG), "Login Activity 1 : " + error.getMessage());
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mSignInButton.setProgress(-1);
                                                }
                                            });
                                        }
                                    }

                                }
                            });
                } else {
                    Snackbar snackbar = Snackbar
                            .make(mScrollViewContainer, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                            .setAction(R.string.try_again, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    attemptLogin();
                                }
                            });
                    snackbar.show();
                }
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSignInButton.setProgress(-1);
                    }
                });
                Snackbar snackbar = Snackbar
                        .make(mScrollViewContainer, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
                snackbar.show();
                Log.e(getString(R.string.TAG), "LoginActivity Exception = " + String.valueOf(e));
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            mAuthTask = null;
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSignInButton.setProgress(-1);
                }
            });

        }

        private void sendNotification() {
            Ion.with(LoginActivity.this)
                    .load(getString(R.string.Resource_Address) + getString(R.string.UpdateToken_Address))
                    .addHeader("Authorization", "Bearer " + userData.Get_UserToken(LoginActivity.this))
                    .setBodyParameter("firebaseToken", deviceToken.trim())
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (result != null && !Objects.equals(result, "[]")) {

                                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
                                String value=tokenRequest.getError();
                                if (value==null)
                                {
                                    testActivation = 1;
                                    writeToFile("1");
                                }


                                else if (value.matches("Account has not been activated yet"))
                                {
                                    testActivation = 0;
                                    writeToFile("0");
                                }
                                else if (value.matches("Phone or password is incorrect") && mPassword.matches("0") && mPhoneNumber.matches("0"))
                                {
                                    testActivation = -1;
                                    writeToFile("-1");
                                    tokenRequest.setSuccess("true");
                                }


                                if (Objects.equals(tokenRequest.getSuccess(), "true")) {
                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mSignInButton.setProgress(-1);
                                        }
                                    });
                                    Snackbar snackbar = Snackbar
                                            .make(mScrollViewContainer, R.string.error_there_was_an_error_please_try_again, Snackbar.LENGTH_LONG);

                                    snackbar.show();

                                    Log.d("FirebaseError", getString(R.string.error_there_was_an_error_in_saving_your_firebase_id));

                                }
                            }
                        }
                    });

        }
    }
}