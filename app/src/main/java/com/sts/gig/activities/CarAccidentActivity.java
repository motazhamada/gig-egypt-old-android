package com.sts.gig.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.classes.DatePickerFragment;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.utilities.DrawableComparer;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.LanguageManager;
import com.sts.gig.utilities.UserData;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Objects;

public class CarAccidentActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{
    private String mCar_ID, mAccident_ID, mAccident_Editable, mAccident_DamageArea;
    private ImageView cTL, cTC, cTR, cFG, cTDL, cTDR, cBDL, cBDR, cBG, cBL, cBC, cBR;
    private com.dd.processbutton.iml.ActionProcessButton submitCarAccidentButton;
    private LinearLayout linearLayout;
    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private UserData userData;
    private boolean[] broken = new boolean[12];
    private String [] broken_Words=new String[12];
    private String broken_Send="";
    private boolean mNew = true;
    private String written_report;
    private TextView dateField;


    public boolean areAllFalse(boolean[] array) {
        for (boolean b : array) if (b) return false;
        return true;
    }

    public String arrayToString(boolean[] array) {
        StringBuilder damageArea = new StringBuilder();

        for (boolean b : array) {
            if (b) {
                damageArea.append("1");
            } else {
                damageArea.append("0");
            }
        }
        return damageArea.toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
        submitCarAccidentButton.setMode(com.dd.processbutton.iml.ActionProcessButton.Mode.PROGRESS);
        submitCarAccidentButton.setProgress(0);
    }

/*
    @Override
    public void onBackPressed()
    {
        if(AccidentsActivity.accident_activity_id==1)
        {
        Intent intent = new Intent(CarAccidentActivity.this, CompensationActivity.class);
        startActivity(intent);
        CarsActivity.Cars_actvity_id=0;
        AccidentsActivity.accident_activity_id=0;
        finish();
        }
        else if(CarsActivity.Cars_actvity_id==1)
        {
            Intent intent = new Intent(CarAccidentActivity.this, CarsActivity.class);
            startActivity(intent);
            CarsActivity.Cars_actvity_id=0;
            AccidentsActivity.accident_activity_id=0;
            finish();

        }

    }
*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        written_report="";

        LanguageManager languageManager = new LanguageManager(CarAccidentActivity.this);
        languageManager.getLanguage();

        setContentView(R.layout.activity_car_accident);
        internetConnection = new InternetConnection();
        userData = new UserData();
        userData.isLogout(this);

        dateField = findViewById(R.id.date_field);
        dateField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(getFragmentManager(), "dateDialog");

            }
        });


        initialize();
        areAllFalse(broken);
        Intent intent = getIntent();
        if (!Objects.equals(intent.getStringExtra("Car_ID"), null)) {
            mNew = true;
            mCar_ID = intent.getStringExtra("Car_ID");
        }
        if (!Objects.equals(intent.getStringExtra("Accident_ID"), null)) {
            mNew = false;
            mAccident_ID = intent.getStringExtra("Accident_ID");
        }
        if (!Objects.equals(intent.getStringExtra("Accident_Editable"), null)) {
            mNew = false;
            mAccident_Editable = intent.getStringExtra("Accident_Editable");
            if (mAccident_Editable.equals("1")) {
                clickListeners();
                submitCarAccidentButton.setVisibility(View.VISIBLE);
            } else {
                submitCarAccidentButton.setVisibility(View.GONE);
            }
        }
        if (!Objects.equals(intent.getStringExtra("Accident_DamageArea"), null)) {
            mAccident_DamageArea = intent.getStringExtra("Accident_DamageArea");
            loadDamageArea();
        }
        if(mNew){
            clickListeners();
        }
    }

    private void loadDamageArea() {
        if (mAccident_DamageArea.length() == 23) {
            if (mAccident_DamageArea.charAt(0) != '0') {
                cTL.setImageResource((R.drawable.c1b));
                broken[0] = true;
            } else {
                cTL.setImageResource((R.drawable.c1));
                broken[0] = false;
            }
            if (mAccident_DamageArea.charAt(2) != '0') {
                cTC.setImageResource((R.drawable.c2b));
                broken[1] = true;
            } else {
                cTC.setImageResource((R.drawable.c2));
                broken[1] = false;
            }
            if (mAccident_DamageArea.charAt(4)!= '0') {
                cTR.setImageResource((R.drawable.c3b));
                broken[2] = true;
            } else {
                cTR.setImageResource((R.drawable.c3));
                broken[2] = false;
            }
            if (mAccident_DamageArea.charAt(6) != '0') {
                cTDL.setImageResource((R.drawable.c4b));
                broken[3] = true;
            } else {
                cTDL.setImageResource((R.drawable.c4));
                broken[3] = false;
            }
            if (mAccident_DamageArea.charAt(8) != '0') {
                cFG.setImageResource((R.drawable.c5b));
                broken[4] = true;
            } else {
                cFG.setImageResource((R.drawable.c5));
                broken[4] = false;
            }

            if (mAccident_DamageArea.charAt(10) != '0') {
                cTDR.setImageResource((R.drawable.c6b));
                broken[5] = true;
            } else {
                cTDR.setImageResource((R.drawable.c6));
                broken[5] = false;
            }

            if (mAccident_DamageArea.charAt(12) != '0') {
                cBDL.setImageResource((R.drawable.c7b));
                broken[6] = true;
            } else {
                cBDL.setImageResource((R.drawable.c7));
                broken[6] = false;
            }

            if (mAccident_DamageArea.charAt(14) != '0') {
                cBG.setImageResource((R.drawable.c8b));
                broken[7] = true;
            } else {
                cBG.setImageResource((R.drawable.c8));
                broken[7] = false;
            }

            if (mAccident_DamageArea.charAt(16)!= '0') {
                cBDR.setImageResource((R.drawable.c9b));
                broken[8] = true;
            } else {
                cBDR.setImageResource((R.drawable.c9));
                broken[8] = false;
            }

            if (mAccident_DamageArea.charAt(18) != '0') {
                cBL.setImageResource((R.drawable.c10b));
                broken[9] = true;
            } else {
                cBL.setImageResource((R.drawable.c10));
                broken[9] = false;
            }

            if (mAccident_DamageArea.charAt(20) != '0') {
                cBC.setImageResource((R.drawable.c11b));
                broken[10] = true;
            } else {
                cBC.setImageResource((R.drawable.c11));
                broken[10] = false;
            }

            if (mAccident_DamageArea.charAt(22) != '0') {
                cBR.setImageResource((R.drawable.c12b));
                broken[11] = true;
            } else {
                cBR.setImageResource((R.drawable.c12));
                broken[11] = false;
            }
        }
    }

    private void popUpEditText() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Comments");

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                written_report=input.getText().toString().trim();

                if (mNew)
                {

                    sendingCarData(getString(R.string.Resource_Address) + getString(R.string.InsertCarAccident_Address), "car_id", mCar_ID);
                }
                else
                {

                    sendingCarData(getString(R.string.Resource_Address) + getString(R.string.UpdateCarAccident_Address), "accident_id", mAccident_ID);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                written_report="";
                if (mNew)
                {

                    sendingCarData(getString(R.string.Resource_Address) + getString(R.string.InsertCarAccident_Address), "car_id", mCar_ID);
                }
                else
                {

                    sendingCarData(getString(R.string.Resource_Address) + getString(R.string.UpdateCarAccident_Address), "accident_id", mAccident_ID);
                }                dialog.cancel();
            }
        });
        builder.setCancelable(false);
        builder.show();

    }

    private void clickListeners() {
        submitCarAccidentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popUpEditText();
              /*  submitCarAccidentButton.setMode(com.dd.processbutton.iml.ActionProcessButton.Mode.ENDLESS);
                if (mNew)
                {

                    sendingCarData(getString(R.string.Resource_Address) + getString(R.string.InsertCarAccident_Address), "car_id", mCar_ID);
                }
                else
                    {

                      sendingCarData(getString(R.string.Resource_Address) + getString(R.string.UpdateCarAccident_Address), "accident_id", mAccident_ID);
                }
                */
            }
        });
        cTL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (DrawableComparer.areDrawablesIdentical(cTL.getDrawable(), getResources().getDrawable(R.drawable.c1))) {
                    cTL.setImageResource((R.drawable.c1b));
                    broken[0] = true;
                } else {
                    cTL.setImageResource((R.drawable.c1));
                    broken[0] = false;
                }
            }

        });

        cTC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cTC.getDrawable(), getResources().getDrawable(R.drawable.c2))) {
                    cTC.setImageResource((R.drawable.c2b));
                    broken[1] = true;
                } else {
                    cTC.setImageResource((R.drawable.c2));
                    broken[1] = false;
                }
            }
        });
        cTR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cTR.getDrawable(), getResources().getDrawable(R.drawable.c3))) {
                    cTR.setImageResource((R.drawable.c3b));
                    broken[2] = true;
                } else {
                    cTR.setImageResource((R.drawable.c3));
                    broken[2] = false;
                }
            }
        });
        cTDL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cTDL.getDrawable(), getResources().getDrawable(R.drawable.c4))) {
                    cTDL.setImageResource((R.drawable.c4b));
                    broken[3] = true;
                } else {
                    cTDL.setImageResource((R.drawable.c4));
                    broken[3] = false;
                }
            }
        });
        cFG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cFG.getDrawable(), getResources().getDrawable(R.drawable.c5))) {
                    cFG.setImageResource((R.drawable.c5b));
                    broken[4] = true;
                } else {
                    cFG.setImageResource((R.drawable.c5));
                    broken[4] = false;
                }
            }
        });
        cTDR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cTDR.getDrawable(), getResources().getDrawable(R.drawable.c6))) {
                    cTDR.setImageResource((R.drawable.c6b));
                    broken[5] = true;
                } else {
                    cTDR.setImageResource((R.drawable.c6));
                    broken[5] = false;
                }
            }
        });
        cBDL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cBDL.getDrawable(), getResources().getDrawable(R.drawable.c7))) {
                    cBDL.setImageResource((R.drawable.c7b));
                    broken[6] = true;
                } else {
                    cBDL.setImageResource((R.drawable.c7));
                    broken[6] = false;
                }
            }
        });
        cBG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cBG.getDrawable(), getResources().getDrawable(R.drawable.c8))) {
                    cBG.setImageResource((R.drawable.c8b));
                    broken[7] = true;
                } else {
                    cBG.setImageResource((R.drawable.c8));
                    broken[7] = false;
                }

            }
        });
        cBDR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cBDR.getDrawable(), getResources().getDrawable(R.drawable.c9))) {
                    cBDR.setImageResource((R.drawable.c9b));
                    broken[8] = true;
                } else {
                    cBDR.setImageResource((R.drawable.c9));
                    broken[8] = false;
                }

            }
        });
        cBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cBL.getDrawable(), getResources().getDrawable(R.drawable.c10))) {
                    cBL.setImageResource((R.drawable.c10b));
                    broken[9] = true;
                } else {
                    cBL.setImageResource((R.drawable.c10));
                    broken[9] = false;
                }

            }
        });
        cBC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cBC.getDrawable(), getResources().getDrawable(R.drawable.c11))) {
                    cBC.setImageResource((R.drawable.c11b));
                    broken[10] = true;
                } else {
                    cBC.setImageResource((R.drawable.c11));
                    broken[10] = false;
                }

            }
        });
        cBR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DrawableComparer.areDrawablesIdentical(cBR.getDrawable(), getResources().getDrawable(R.drawable.c12))) {
                    cBR.setImageResource((R.drawable.c12b));
                    broken[11] = true;
                } else {
                    cBR.setImageResource((R.drawable.c12));
                    broken[11] = false;
                }
            }
        });
    }

    private void initialize() {
        linearLayout = findViewById(R.id.carAccident_linearLayout);
        cTL = findViewById(R.id.car_top_lift);
        cTC = findViewById(R.id.car_top_center);
        cTR = findViewById(R.id.car_top_right);
        cFG = findViewById(R.id.car_front_glass);
        cTDL = findViewById(R.id.car_top_dor_left);
        cTDR = findViewById(R.id.car_top_dor_right);
        cBDL = findViewById(R.id.car_bottom_dor_left);
        cBDR = findViewById(R.id.car_bottom_dor_right);
        cBG = findViewById(R.id.car_back_glass);
        cBL = findViewById(R.id.car_bottom_left);
        cBC = findViewById(R.id.car_bottom_center);
        cBR = findViewById(R.id.car_bottom_right);
        submitCarAccidentButton = findViewById(R.id.submitCarAccident_button);
    }

    private void sendingCarData(final String mUrl, final String mParameter, final String mParameterValue) {
        if (areAllFalse(broken)) {
            Snackbar snackbar = Snackbar
                    .make(linearLayout, R.string.error_there_is_no_broken_parts_selected, Snackbar.LENGTH_LONG);
            snackbar.show();
            submitCarAccidentButton.setProgress(0);
            return;
        }
        try {

            if (internetConnection.isNetworkAvailable(CarAccidentActivity.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        submitCarAccidentButton.setProgress(30);
                    }
                });
                // Instantiate the RequestQueue.
                ////////////////
                Substitute(broken);

                //

                if(written_report.equals(""))
                {
                    written_report = "No comments...    ";
                }

                //

                Ion.with(CarAccidentActivity.this)
                        .load(mUrl)
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(CarAccidentActivity.this))
                        .setBodyParameter(mParameter, mParameterValue)
                        .setBodyParameter("damageArea", broken_Send)
                        .setBodyParameter("damageText",written_report)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
                                        if (Objects.equals(tokenRequest.getSuccess(), "true")) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    submitCarAccidentButton.setProgress(100);
                                                }
                                            });
                                            Intent intent = new Intent(CarAccidentActivity.this, HomeActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    submitCarAccidentButton.setProgress(-1);
                                                }
                                            });
                                            Snackbar snackbar = Snackbar
                                                    .make(linearLayout, tokenRequest.getError(), Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception error) {
                                        Log.e(getString(R.string.TAG), "CarAccident Activity 1 : " + error.getMessage());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                submitCarAccidentButton.setProgress(-1);
                                            }
                                        });
                                    }
                                }
                            }
                        });
            } else {
                Snackbar snackbar = Snackbar
                        .make(linearLayout, R.string.error_no_internet_connection, Snackbar.LENGTH_LONG)
                        .setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sendingCarData(mUrl, mParameter, mParameterValue);
                            }
                        });
                snackbar.show();
            }
        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    submitCarAccidentButton.setProgress(-1);
                }
            });
            Snackbar snackbar = Snackbar
                    .make(linearLayout, R.string.error_there_is_an_error_try_again_later, Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), "CarAccidentActivity Exception = " + String.valueOf(e));
        }
    }
    private void Substitute(boolean[]B)
    {
        String [] W=new String[12];
        // Words chosen must match the car
        W[0]="A";
        W[1]="B";
        W[2]="C";
        W[3]="D";
        W[4]="E";
        W[5]="F";
        W[6]="G";
        W[7]="H";
        W[8]="I";
        W[9]="J";
        W[10]="K";
        W[11]="L";
        for(int i=0;i< B.length;i++)
        {
            if(B[i]==true)
                broken_Words[i]=W[i];
            else
                broken_Words[i]="0";
        }
        for(int i=0;i<broken_Words.length;i++)
        {
            if(i<broken_Words.length-1)
                broken_Send+=broken_Words[i]+",";
            else
                broken_Send+=broken_Words[i];
        }

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, i);
        calendar.set(Calendar.MONTH, i1);
        calendar.set(Calendar.DAY_OF_MONTH, i2);

        String currentDateString = DateFormat.getDateInstance().format(calendar.getTime());

        dateField.setText(currentDateString);
        dateField.setTextColor(Color.BLACK);

    }
}
