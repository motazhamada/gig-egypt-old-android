package com.sts.gig.services;

import android.content.Context;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;


public class ServiceFactory {

    public static void getData(Context context, String url, final RequestListener listener, final int id)
    {
        Ion.with(context)
                .load(url).addHeader("Content-Type"," application/x-www-form-urlencoded")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the Result or error
                        try {
                            listener.onSuccess(result,id);
                        } catch (Exception error) {
                            listener.onFailure(e,id);
                        }
                    }

                });

    }
    public static void getData(Context context, String url, final RequestListener listener, final int id,final int id2)
    {
//        Ion.with(context)
//                .load("https://koush.clockworkmod.com/test/echo")
//                .setBodyParameter("goop", "noop")
//                .setBodyParameter("foo", "bar")
//                .asJsonObject()
//                .setCallback(...)
        Ion.with(context)
                .load(url).addHeader("Content-Type"," application/x-www-form-urlencoded")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the Result or error
                        try {
                            listener.onSuccess(result,id);
                        } catch (Exception error) {
                            listener.onFailure(e,id);
                        }
                    }

                });

    }
}
