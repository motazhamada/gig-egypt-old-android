package com.sts.gig.services;

public interface RequestListener {

    void onSuccess(Object object,int id);
//    void onFailure(int errorCode,int id);
    void onFailure(Exception error,int id);

}
