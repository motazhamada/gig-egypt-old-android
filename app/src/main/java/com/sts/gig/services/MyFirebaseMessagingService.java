package com.sts.gig.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sts.gig.R;
import com.sts.gig.activities.AccidentsActivity;
import com.sts.gig.activities.HomeActivity;
import com.sts.gig.activities.MessagesActivity;
import com.sts.gig.activities.NewsActivity;
import com.sts.gig.classes.TokenRequest;
import com.sts.gig.utilities.InternetConnection;
import com.sts.gig.utilities.UserData;

import java.util.Objects;

/**
 * Created by moataz on 3/10/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private InternetConnection internetConnection;
    private TokenRequest tokenRequest;
    private UserData userData;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        sendRegistrationToServer(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.d(getString(R.string.TAG), "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            // Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            String title = remoteMessage.getNotification().getTitle();
            String messageBody = remoteMessage.getNotification().getBody();
            String clickAction = remoteMessage.getNotification().getClickAction();
            sendNotification(title, messageBody, clickAction);
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void handleNow() {
    }

    private void scheduleJob() {
    }

    private void sendNotification(String title, String messageBody, String clickAction) {
        Intent intent;
        userData = new UserData();
        if(!userData.isLogin(getApplicationContext())){
            return;
        }
        if (clickAction.equals("action_news")) {
            intent = new Intent(this, NewsActivity.class);
        } else if (clickAction.equals("action_chat")) {
            intent = new Intent(this, MessagesActivity.class);
        } else if (clickAction.equals("action_accident")) {
            intent = new Intent(this, AccidentsActivity.class);
        } else {
            intent = new Intent(this, HomeActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_gig)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        assert notificationManager != null;
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void sendRegistrationToServer(final String refreshedToken) {
        try {
            internetConnection = new InternetConnection();
            userData = new UserData();

            if (internetConnection.isNetworkAvailable(getApplicationContext())) {
                Ion.with(getApplicationContext())
                        .load(getString(R.string.Resource_Address) + getString(R.string.UpdateToken_Address))
                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(getApplicationContext()))
                        .setBodyParameter("firebaseToken", refreshedToken.trim())
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (result != null && !Objects.equals(result, "[]")) {

                                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                    tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
                                    if (Objects.equals(tokenRequest.getSuccess(), "true")) {

                                    } else {
//
//                                        Snackbar snackbar = Snackbar
//                                                .make(mScrollViewContainer, "there was an error in saving your firebase id", Snackbar.LENGTH_LONG);
//                                        snackbar.show();
                                    }
                                }
                            }
                        });

            } else {
                userData.Clear_UserData(getApplicationContext());
            }
        } catch (Exception e) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    mSignInButton.setProgress(-1);
//                }
//            });
//            Snackbar snackbar = Snackbar
//                    .make(mScrollViewContainer, "there is an Error try again later.", Snackbar.LENGTH_LONG);
//            snackbar.show();
            Log.e(getString(R.string.TAG), "MyFirebaseMessagingService Exception = " + String.valueOf(e));

        }
    }
}
